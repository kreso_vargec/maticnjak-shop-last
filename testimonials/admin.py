from django.contrib import admin
from parler.admin import TranslatableAdmin
from cms.admin.placeholderadmin import FrontendEditableAdminMixin, PlaceholderAdminMixin
from adminsortable2.admin import SortableAdminMixin
from testimonials.models import Testimonial


@admin.register(Testimonial)
class TestimonialAdmin(SortableAdminMixin, FrontendEditableAdminMixin, PlaceholderAdminMixin, TranslatableAdmin):
    frontend_editable_fields = [
        'name',
    ]

    list_display = ['name']

    readonly_fields = ['created', 'modified']

    fieldsets = [
        (None, {'fields': ['name', 'description', 'image']}),
        (None, {'fields': [('created', 'modified')]})
    ]
