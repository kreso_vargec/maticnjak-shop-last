from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.toolbar.items import Break, SubMenu
from django.core.urlresolvers import reverse

from testimonials.models import Testimonial

try:
    from cms.cms_toolbars import ADMIN_MENU_IDENTIFIER, ADMINISTRATION_BREAK
except ImportError:
    from cms.cms_toolbar import ADMIN_MENU_IDENTIFIER, ADMINISTRATION_BREAK


class TestimonialsToolbar(CMSToolbar):
    supported_apps = (
        'testimonials',
    )

    watch_models = [Testimonial]

    def populate(self):
        admin_menu = self.toolbar.get_menu(ADMIN_MENU_IDENTIFIER)

        if admin_menu:
            position = admin_menu.get_alphabetical_insert_position(_('Testimonials'), SubMenu)
            if not position:
                position = admin_menu.find_first(Break, identifier=ADMINISTRATION_BREAK) + 1
                admin_menu.add_break('testimonial-break', position=position)

            menu = admin_menu.get_or_create_menu('testimonials-admin-menu', _('Testimonials'), position=position)

            menu.add_sideframe_item(_('Testimonials list'), url=reverse('admin:testimonials_testimonial_changelist'))
            menu.add_modal_item(_('Add new testimonial'), url=reverse('admin:testimonials_testimonial_add'))
            menu.add_break()

toolbar_pool.register(TestimonialsToolbar)
