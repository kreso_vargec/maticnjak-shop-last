from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from parler.models import TranslatableModel, TranslatedFields
from filer.fields.image import FilerImageField


@python_2_unicode_compatible
class Testimonial(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(
            verbose_name=_('Name'),
            max_length=128
        ),
        description=models.TextField(
            verbose_name=_('Description'),
            blank=True,
            null=True
        )
    )

    image = FilerImageField(
        verbose_name=_('Image'),
        related_name='+',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    created = models.DateTimeField(
        verbose_name=_('Created'),
        auto_now_add=True
    )

    modified = models.DateTimeField(
        verbose_name=_('Modified'),
        auto_now=True
    )

    order = models.PositiveIntegerField(
        verbose_name=_('Sort'),
        default=0
    )

    class Meta:
        verbose_name = _('Testimonial')
        verbose_name_plural = _('Testimonialas')
        ordering = ['order']

    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
