# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import ListView, DeleteView
from parler.views import TranslatableSlugMixin

from testimonials.models import Testimonial


class TestimonialListView(ListView):
    model = Testimonial

    def get_queryset(self):
        return self.model.objects.all()
