# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template
from testimonials.models import Testimonial

register = template.Library()


@register.simple_tag
def get_testimonials(limit=None):
    return Testimonial.objects.all()[:limit]
