# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-11-19 13:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0020_old_tree_cleanup'),
        ('locations', '0003_remove_webshop_content'),
    ]

    operations = [
        migrations.CreateModel(
            name='Store',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='locations_store', serialize=False, to='cms.CMSPlugin')),
                ('city', models.CharField(max_length=255, verbose_name='Name')),
                ('adress', djangocms_text_ckeditor.fields.HTMLField(blank=True, max_length=255, null=True, verbose_name='Adress')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Stores',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='locations_stores', serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
