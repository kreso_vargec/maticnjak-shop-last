# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from djangocms_text_ckeditor.fields import HTMLField

from cms.models import CMSPlugin
from filer.fields.image import FilerImageField


@python_2_unicode_compatible
class WebShops(CMSPlugin):
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=255,
    )

    def __str__(self):
        return self.title

@python_2_unicode_compatible
class WebShop(CMSPlugin):
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=255,
    )

    link = models.CharField(
        verbose_name=_('Link'),
        max_length=255,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Stores(CMSPlugin):
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=255,
    )

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class Store(CMSPlugin):
    city = models.CharField(
        verbose_name=_('Name'),
        max_length=255,
    )

    adress = HTMLField (
        verbose_name=_('Adress'),
        blank=True,
        null=True
    )

    def __str__(self):
        return self.city
