# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import WebShops, WebShop, Stores, Store


@plugin_pool.register_plugin
class WebShopsPlugin(CMSPluginBase):
    model = WebShops
    name = _('Web shops')
    allow_children = True
    render_template = 'locations/web_shops.html'
    child_classes = ['WebShopPlugin']


@plugin_pool.register_plugin
class WebShopPlugin(CMSPluginBase):
    model = WebShop
    name = _('Item')
    render_template = 'locations/web_shop.html'
    require_parent = True
    parent_classes = ['WebShopsPlugin']


@plugin_pool.register_plugin
class StoresPlugin(CMSPluginBase):
    model = Stores
    name = _('Stores')
    allow_children = True
    render_template = 'locations/stores.html'
    child_classes = ['StorePlugin']


@plugin_pool.register_plugin
class StorePlugin(CMSPluginBase):
    model = Store
    name = _('Store')
    render_template = 'locations/store.html'
    require_parent = True
    parent_classes = ['StoresPlugin']
