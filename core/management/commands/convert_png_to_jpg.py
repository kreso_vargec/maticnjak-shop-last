from django.core.management.base import BaseCommand, CommandError
from filer.models import Image as ImageModel

from PIL import Image
import os

class Command(BaseCommand):

    def handle(self, *args, **options):
        png_ext = 'png'
        for image_instance in ImageModel.objects.all():
            if not (image_instance.extension == png_ext and os.path.isfile(image_instance.path)):
                continue

            new_path = image_instance.path[:-len(png_ext)] + 'jpg'
            png_img = Image.open(image_instance.path)
            alpha = png_img.convert('RGBA').split()[-1]
            jpg_img = Image.new("RGBA", png_img.size, (255,)*4)
            jpg_img.paste(png_img, mask=alpha)
            jpg_img.convert('RGB').save(new_path)
            os.remove(image_instance.path)
            old_name = image_instance.file.name
            image_instance.file.name = image_instance.file.name[:-len(png_ext)] + 'jpg'
            image_instance.save()

            self.stdout.write(self.style.SUCCESS(
                'Successfully converted png image "%s" to jpg' % old_name))