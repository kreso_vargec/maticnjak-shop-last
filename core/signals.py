from django.db.models.signals import post_save
from filer.fields.image import FilerImageField

from PIL import Image
import os


def convert_png_to_jpg(sender, instance, **kwargs):
    image_field = None
    for field in instance._meta.fields:
        if isinstance(field, FilerImageField):
            image_field = field
    if image_field == None:
        return

    png_ext = 'png'
    instance_image_attr = getattr(instance, image_field.name)
    if not (instance_image_attr.extension == png_ext and os.path.isfile(instance_image_attr.path)):
        return

    new_path = instance_image_attr.path[:-len(png_ext)] + 'jpg'
    png_img = Image.open(instance_image_attr.path)
    alpha = png_img.convert('RGBA').split()[-1]
    jpg_img = Image.new("RGBA", png_img.size, (255,)*4)
    jpg_img.paste(png_img, mask=alpha)
    jpg_img.convert('RGB').save(new_path)
    os.remove(instance_image_attr.path)
    instance_image_attr.file.name = instance_image_attr.file.name[:-len(png_ext)] + 'jpg'
    instance_image_attr.save()


post_save.connect(convert_png_to_jpg)
