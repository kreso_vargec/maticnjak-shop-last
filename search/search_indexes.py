# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.html import strip_tags
from django.utils import translation

from haystack import indexes

from shopit.models.product import ProductTranslation


class ProductIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Search index.
    """
    text = indexes.CharField(document=True)
    autocomplete = indexes.EdgeNgramField()
    language = indexes.CharField(model_attr='language_code')

    def get_model(self):
        return ProductTranslation

    def index_queryset(self, using=None):
        """Only groups and variants."""
        return self.get_model().objects.filter(master__kind__in=[0, 1], master__active=True)

    def prepare(self, obj):
        """Activate language before accessing translation master attributes."""
        translation.activate(obj.language_code)
        return super(ProductIndex, self).prepare(obj)

    def prepare_text(self, obj):
        text_plugins = obj.master.content.get_plugins().filter(plugin_type='TextPlugin', language=obj.language_code)

        text = ''
        for plugin in text_plugins:
            try:
                body = plugin.get_plugin_instance()[0].body
                text += strip_tags(body)
            except AttributeError:
                pass

        # text_plugins = ' '.join([strip_tags(x.get_plugin_instance()[0].body) for x in text_plugins])

        return ' '.join([
            str(obj.master.code),
            obj.name, obj.slug,
            obj.master.description,
            text,
        ]).rstrip()

    def prepare_autocomplete(self, obj):
        return ' '.join([obj.name, str(obj.master.code)])
