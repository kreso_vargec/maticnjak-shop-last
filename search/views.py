# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from drf_haystack.filters import HaystackAutocompleteFilter, HaystackHighlightFilter
from drf_haystack.serializers import HaystackSerializer
from drf_haystack.viewsets import HaystackViewSet
from rest_framework import serializers
from shopit.serializers import ProductSummarySerializer

from .search_indexes import ProductIndex


class SearchSerializer(HaystackSerializer):
    """
    Serializer for search result.
    """
    product = serializers.SerializerMethodField()

    class Meta:
        index_classes = [ProductIndex]
        fields = ['text', 'product', 'autocomplete']
        ignore_fields = ['autocomplete']
        field_aliases = {'q': 'autocomplete'}

    def get_product(self, obj):
        if obj.object and obj.object.master:
            return ProductSummarySerializer(obj.object.master, context=self.context).data


class SearchViewSet(HaystackViewSet):
    """
    Search ViewSet.
    """
    serializer_class = SearchSerializer
    filter_backends = [HaystackHighlightFilter, HaystackAutocompleteFilter]

    def filter_queryset(self, queryset):
        """Return only results in current language."""
        queryset = super(SearchViewSet, self).filter_queryset(queryset)
        return queryset.filter(language=self.request.LANGUAGE_CODE)
