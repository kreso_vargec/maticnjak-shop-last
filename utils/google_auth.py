# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import


def make_superuser(backend, user, response, *args, **kwargs):
    """Make iconisagency google apps accounts into superusers"""
    if backend.name.startswith('google') and user.email.split('@').pop() == 'iconisagency.com':
        if not user.is_staff or not user.is_superuser or not user.is_active:
            user.is_staff = user.is_superuser = user.is_active = True
            user.save()
