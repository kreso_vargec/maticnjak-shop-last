# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.utils.deprecation import MiddlewareMixin
from re import compile


EXEMPT_URLS = [
    compile('admin/'),
    compile('demo/'),
    compile('social/'),
]


class DemoMiddleware(MiddlewareMixin):
    """Redirect to /demo/ if not logged in"""
    def process_request(self, request):
        assert hasattr(request, 'user'), "User not found in request."
        if not request.user.is_authenticated:
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                return HttpResponseRedirect('/demo/?next={0}'.format(request.path))
