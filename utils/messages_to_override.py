# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext as _


MESSAGES_TO_OVERIDDE = [
    # django.contrib.auth
    _("Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."),
    _("The password is too similar to the %(verbose_name)s."),
    _("Your password can't be too similar to your other personal information."),
    _("This password is too common."),
    _("Your password can't be a commonly used password."),
    _("Enter a valid username. This value may contain only English letters, numbers, and @/./+/-/_ characters."),
    _("Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters."),

    # shop, shopit
    _("Order Canceled"),
    _("Refund payment"),
    _("Payment confirmed"),
    _("Ship goods"),
    _("Pay in advance"),
    _("Awaiting a forward fund payment"),
    _("Prepayment deposited"),
    _("No Payment Required"),
    _("Acknowledge Payment"),
    _("Deposited too little"),

    # shop_wspay
    _("Paid using WSPay"),

    # phonenumber_field
    _('Enter a valid phone number.'),
]
