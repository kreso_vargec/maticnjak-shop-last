# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import Library
from django.conf import settings

register = Library()


@register.simple_tag
def get_setting(name):
    """
    Return settings from `django.conf.settings`.
    """
    return getattr(settings, name, None)
