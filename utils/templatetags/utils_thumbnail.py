# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import math

from django.template import Library

register = Library()


@register.filter
def thumbnail_bigger_size(value, factor=1.5):
    """
    Scales size value (200x200) by the given factor.
    """
    width = int(int(value.split('x')[0]) * factor)
    height = int(int(value.split('x')[1]) * factor)
    return '{}x{}'.format(width, height)


@register.simple_tag
def get_optimal_thumbnail_size(image, maxsize):
    """
    Returns best image size for the given `maxsize` with respect for
    image aspect ratio.

    {% get_optimal_thumbnail_size image "200x200" as thumb_size %}
    """
    maxwidth = int(maxsize.split('x')[0])
    maxheight = int(maxsize.split('x')[1])
    try:
        width = math.floor(image.width)
        height = math.floor(image.height)
        ratio = min(maxwidth / width, maxheight / height)
        size = '{}x{}'.format(int(math.floor(width * ratio)), int(math.floor(height * ratio)))
        return size
    except AttributeError:
        return maxsize
