# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import Library
from django.utils import six

from filer.models.foldermodels import Folder
from filer.models.imagemodels import Image

register = Library()


@register.simple_tag
def get_image(name, folder=None):
    """
    Returns image with the given name, or None.
    """
    if folder:
        images = get_folder_images(folder)
    else:
        images = Image.objects.all()
    return images.filter(name=name).first()


@register.simple_tag
def get_folder_images(folder):
    """
    Returns all images from the given folder instance or folder name.
    """
    if isinstance(folder, six.string_types):
        try:
            folder = Folder.objects.filter(name=folder)[0]
        except IndexError:
            return None

    return folder.files.instance_of(Image).order_by('name', 'original_filename')
