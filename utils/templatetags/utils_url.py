# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import NoReverseMatch
from django.template import Library

from parler.templatetags.parler_tags import get_translated_url

try:
    from urllib.parse import urlparse, parse_qs
except ImportError:
    from urlparse import urlparse, parse_qs

register = Library()


@register.simple_tag(takes_context=True)
def query_transform(context, *args, **kwargs):
    """
    Appends or updates current query string. Can be used as pairs
    passing in every second arg as value so that key can be dynamic.
    It also supports the kwargs format.

    {% query_transform <key> <value> <key> <value> <key>=<value> %}
    """
    get = context['request'].GET.copy()
    if args:
        args_keys = [args[i] for i in range(len(args)) if i % 2 == 0]
        args_vals = [args[i] for i in range(len(args)) if i % 2 != 0]
        for i in range(len(args_vals)):
            k, v = args_keys[i], args_vals[i]
            if v == '' and k in get:
                del get[k]
            elif v != '':
                get[k] = v
    for k, v in kwargs.items():
        if v == '' and k in get:
            del get[k]
        elif v != '':
            get[k] = v
    return get.urlencode()


@register.simple_tag
def get_absolute_url_with_fallback(obj, fallback=''):
    """
    Used in place of `obj.get_absolute_url` to handle `NoReverseMatch` error
    case, and return a default 'fallback'.
    """
    try:
        return obj.get_absolute_url()
    except (AttributeError, NoReverseMatch):
        return fallback


@register.simple_tag(takes_context=True)
def get_translated_url_with_fallback(context, lang_code, obj=None, fallback=''):
    """
    Wrapper for `get_translated_url` to handle 'NoReverseMatch' exception.
    """
    try:
        return get_translated_url(context, lang_code, obj)
    except (NoReverseMatch, TypeError):
        return fallback


@register.simple_tag
def get_video_data_from_url(url):
    """
    Returns a dict containing video provider and embed_id. Accepts embed urls from
    youtube or vimeo.

    https://www.youtube.com/embed/ML6-FeTQdcc
    https://vimeo.com/channels/staffpicks/196051853
    """
    data = urlparse(url)

    try:
        provider = data.netloc.split('.')[-2]
        path = list([x for x in data.path.split('/') if x])
        embed_id = path[-1]
    except IndexError:
        return None

    # Handle youtube standard url.
    if provider == 'youtube' and 'embed' not in path:
        try:
            embed_id = parse_qs(data.query).get('v')[0]
        except TypeError:
            return None

    return {
        'provider': provider,
        'embed_id': embed_id,
    }
