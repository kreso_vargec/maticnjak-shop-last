# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import Library
from django.utils.encoding import force_text
import math
from django.utils.html import format_html


register = Library()


@register.filter
def stringify(value):
    """
    Force unicode string.
    """
    return force_text(value)


@register.filter
def startswith(value, text):
    """
    Returns if `value` startswith `text`.
    """
    return value.startswith(text)


@register.filter
def endswith(value, text):
    """
    Returns if `value` endswith `text`.
    """
    return value.endswith(text)


@register.filter
def concat(value, text):
    """
    Concatenats given text onto a value.
    """
    return '{0}{1}'.format(value, text)


@register.filter
def format_page_title(value, round_up=False):
    """Wraps the second half of the title in a <strong> tag."""
    words = value.split(' ')

    middle = float(len(words)) / 1.75
    middle = math.ceil(middle) if round_up else math.floor(middle)
    middle = int(middle)

    words.insert(middle, '<strong>')
    words.append('</strong>')
    return format_html(' '.join(words))
