# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template.library import Library, import_library

register = Library()

modules = ['core', 'cms', 'text', 'debug', 'filer', 'thumbnail', 'url']

for module in modules:
    library = import_library('utils.templatetags.utils_%s' % module)
    register.filters.update(library.filters)
    register.tags.update(library.tags)
