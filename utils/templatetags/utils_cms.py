# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import Library

from cms.models.pagemodel import Page

register = Library()


@register.simple_tag(takes_context=True)
def get_page(context, id, language=None):
    """
    Returns page by the given `reverse_id`.
    """
    request = context['request']
    if not language:
        language = request.LANGUAGE_CODE
    try:
        return Page.objects.filter(reverse_id=id, languages__icontains=language)[0]
    except IndexError:
        return None
