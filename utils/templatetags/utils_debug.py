# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import Library

register = Library()


@register.simple_tag
def var_dump(value, show_dir=False):
    """
    Raises an arror with the given value, for debugging.
    """
    data = dir(value) if show_dir else value
    raise ValueError(data)
