{% load i18n l10n %}{% blocktrans with order_number=order.number %}Vaša narudžba broj: {{ order_number }}{% endblocktrans %}
<br />
<hr />
{% for item in order.items %}<p>{{ item.quantity }} × {{ item.summary.name }}{% for _, extra_row in item.extra.rows %}
{{ extra_row.label }}: {{ extra_row.amount }}{% endfor %}
{% blocktrans with line_total=item.line_total %}Cijena: {{ line_total }}{% endblocktrans %}
</p>
{% endfor %
<hr />
<p>
{% blocktrans with subtotal=order.subtotal %}Cijena narudžbe: {{ subtotal }}{% endblocktrans %}{% for _, extra_row in order.extra.rows %}<br />
{{ extra_row.label }}: {{ extra_row.amount }}{% endfor %}
<br />
{% blocktrans with total=order.total %}Ukupno: {{ total }}{% endblocktrans %}
</p>


