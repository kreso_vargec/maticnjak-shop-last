#!/bin/sh
# Remove backups older than 10 days.
if [[ -n "$DB_DUMP_DEBUG" ]]; then
  set -x
fi

if [ -d "$DUMPDIR" ]; then
  find $DUMPDIR -mtime +10 -type f -delete -exec echo "Removing backup: {}" \;
else
  echo "ERROR: Backup directory ${DUMPDIR} does not exist!"
fi
