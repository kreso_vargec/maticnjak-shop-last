# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from djangocms_text_ckeditor.fields import HTMLField

from cms.models import CMSPlugin
from filer.fields.image import FilerImageField


@python_2_unicode_compatible
class Facts(CMSPlugin):
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=255,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.title

@python_2_unicode_compatible
class FactsItem(CMSPlugin):
    content = HTMLField(
        verbose_name=_('Content'),
    )

    def __str__(self):
        return str(self.pk)
