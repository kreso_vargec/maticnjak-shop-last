# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import Facts, FactsItem


@plugin_pool.register_plugin
class FactsPlugin(CMSPluginBase):
    model = Facts
    name = _('Facts')
    allow_children = True
    render_template = 'djangocms_facts/facts.html'
    child_classes = ['FactsItemPlugin']


@plugin_pool.register_plugin
class FactsItemPlugin(CMSPluginBase):
    model = FactsItem
    name = _('Facts item')
    render_template = 'djangocms_facts/facts_item.html'
    require_parent = True
    parent_classes = ['FactsPlugin']
