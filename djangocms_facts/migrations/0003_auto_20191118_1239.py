# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-11-18 11:39
from __future__ import unicode_literals

from django.db import migrations
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('djangocms_facts', '0002_factsitem_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='factsitem',
            name='name',
        ),
        migrations.AlterField(
            model_name='factsitem',
            name='content',
            field=djangocms_text_ckeditor.fields.HTMLField(verbose_name='Content'),
        ),
    ]
