FROM python:2.7.15-alpine3.7

# Install build dependencies.
RUN apk add --no-cache --virtual .build-deps build-base linux-headers nodejs yarn

# Install server dependencies.
RUN apk add --no-cache jpeg-dev zlib-dev libxml2-dev libxslt-dev musl-dev libffi-dev openssl-dev gettext

# Install uwsgi & poetry.
RUN pip install uwsgi poetry==0.12.17
RUN poetry config settings.virtualenvs.create false

# Set workdir.
WORKDIR /home/web/code

# Create `web` user.
RUN adduser -D -h /home/web web

# Create static, media & locale directories.
ENV DJANGO_STATIC_ROOT /home/web/public/static
ENV DJANGO_MEDIA_ROOT /home/web/public/media
RUN mkdir -p $DJANGO_STATIC_ROOT/CACHE $DJANGO_MEDIA_ROOT /home/web/code/locale
RUN chown -R web:web $DJANGO_STATIC_ROOT/CACHE

# Specify volumes.
VOLUME /home/web/public
VOLUME /home/web/code/locale

# Install app requirements.
COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev

# Add temp packages.
# RUN apk add --no-cache git
# RUN mkdir -p /home/web/apps
# RUN cd /home/web/apps && git clone https://github.com/dinoperovic/djangoshop-wspay.git
# RUN cd /usr/local/lib/python2.7/site-packages && ln -s /home/web/apps/djangoshop-wspay/shop_wspay
# RUN apk del git

# Install packages.
COPY package.json yarn.lock webpack.config.js .babelrc postcss.config.js ./
RUN yarn --pure-lockfile

# Build static bundle.
COPY assets ./assets
RUN yarn build

# Clean build dependencies.
RUN apk del .build-deps
RUN rm -rf node_modules

# Copy code.
COPY . .

# Clean assets files.
RUN rm -rf assets

# Add permissions for `web` user.
RUN chown -R web:web /home/web/code

EXPOSE 8000

# Run django stuff then uwsgi server.
CMD /bin/sh -c "\
python manage.py collectstatic --no-input && \
python manage.py makemessages -a && \
uwsgi --logto /tmp/mylog.log --ini conf/uwsgi.ini"
