# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from configurations import Configuration, values


def _(s): return s


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Common(Configuration):
    """
    Common project settings
    """
    SECRET_KEY = values.SecretValue()
    SITE_ID = values.IntegerValue(1)
    # ALLOWED_HOSTS = values.ListValue([])

    # Application definition
    INSTALLED_APPS = [
        'core.apps.CoreConfig',

        'djangocms_admin_style',
        'admin_shortcuts',
        'django.contrib.admin',
        'email_auth',  # required by django-shop.
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.sites',
        'django.contrib.sitemaps',
        'cms',
        'mptt',
        'menus',
        'sekizai',
        'treebeard',
        'filer',
        'easy_thumbnails',
        'djangocms_text_ckeditor',
        'djangocms_link',
        'djangocms_picture',
        'djangocms_file',
        'djangocms_video',
        'djangocms_gallery',
        'djangocms_quote',
        'djangocms_blocks',
        'djangocms_facts',

        'webpack_loader',
        'compressor',
        'parler',
        'rosetta',
        'utils',
        'colorfield',
        'page_images',
        'blogit',
        'blogit_extra',
        'locations',
        'recipes',
        'contact',
        'dealership',
        'media',
        'testimonials',

        # django-shop
        'adminsortable2',
        'cmsplugin_cascade',
        'django_fsm',
        'fsm_admin',
        'post_office',
        'rest_auth',
        'rest_framework',
        'rest_framework.authtoken',
        'shop',

        # social auth
        'social_django',
        'social_extra',

        # shopit
        'shopit',
        'shopit_extra',
        'shop_wspay',

        # search
        'search',
        'haystack',

        'clear_cache',
    ]

    MIDDLEWARE = [
        'django.middleware.cache.UpdateCacheMiddleware',
        'django.middleware.gzip.GZipMiddleware',
        'htmlmin.middleware.HtmlMinifyMiddleware',
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'django.middleware.locale.LocaleMiddleware',
        'django.contrib.sites.middleware.CurrentSiteMiddleware',
        'cms.middleware.page.CurrentPageMiddleware',
        'cms.middleware.user.CurrentUserMiddleware',
        'cms.middleware.toolbar.ToolbarMiddleware',
        'cms.middleware.language.LanguageCookieMiddleware',
        'cms.middleware.utils.ApphookReloadMiddleware',
        # Overriden `SocialAuthExceptionMiddleware` middleware.
        # 'social_django.middleware.SocialAuthExceptionMiddleware',
        'social_extra.middleware.SocialAuthExceptionMiddleware',

        # Replacement for `shop.middleware.CustomerMiddleware`.
        'shopit_extra.middleware.CustomerMiddleware',
        'django.middleware.cache.FetchFromCacheMiddleware',
        'htmlmin.middleware.MarkRequestMiddleware',
    ]

    ROOT_URLCONF = 'project.urls'

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [os.path.join(BASE_DIR, 'templates')],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.template.context_processors.i18n',
                    'django.template.context_processors.media',
                    'django.template.context_processors.static',
                    'django.template.context_processors.tz',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                    'cms.context_processors.cms_settings',
                    'sekizai.context_processors.sekizai',
                    'shop.context_processors.customer',
                    'social_django.context_processors.backends',
                    'social_django.context_processors.login_redirect',
                ],
            },
        },
    ]

    WSGI_APPLICATION = 'project.wsgi.application'

    SILENCED_SYSTEM_CHECKS = ['auth.W004']

    DATABASES = values.DatabaseURLValue('sqlite:///{}'.format(os.path.join(BASE_DIR, 'db.sqlite3')))

    CACHES = values.CacheURLValue('locmem://')

    # Authentication
    AUTH_USER_MODEL = 'email_auth.User'
    AUTHENTICATION_BACKENDS = [
        'django.contrib.auth.backends.ModelBackend',
        'allauth.account.auth_backends.AuthenticationBackend',
    ]
    AUTH_PASSWORD_VALIDATORS = [
        {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
        {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
        {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
        {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
    ]
    LOGIN_URL = 'shopit-account-login'
    LOGOUT_URL = 'shopit-account-logout'
    LOGIN_REDIRECT_URL = 'shopit-account-detail'
    LOGIN_ERROR_URL = LOGIN_URL

    # social_django
    SOCIAL_AUTH_RAISE_EXCEPTIONS = False
    SOCIAL_AUTH_BACKEND_ERROR_URL = LOGIN_ERROR_URL
    SOCIAL_AUTH_PIPELINE = (
        'social_core.pipeline.social_auth.social_details',
        'social_core.pipeline.social_auth.social_uid',
        'social_core.pipeline.social_auth.auth_allowed',
        'social_core.pipeline.social_auth.social_user',
        # Not needed since `create_customer` generates own username.
        # 'social_core.pipeline.user.get_username',
        'social_core.pipeline.social_auth.associate_by_email',
        # Replace user creation with customer.
        # 'social_core.pipeline.user.create_user',
        'social_extra.pipeline.customer.create_customer',

        'social_core.pipeline.social_auth.associate_user',
        'social_core.pipeline.social_auth.load_extra_data',
        'social_core.pipeline.user.user_details',
        # Add success message when logged in.
        'social_extra.pipeline.message.success_message',
    )

    # Email
    ADMINS = [('Maticnjak root', 'prodaja@maticnjak.hr')]
    MANAGERS = [
        ('Maticnjak prodaja', 'prodaja@maticnjak.hr'),
    ]
    EMAIL_USE_TLS = True
    EMAIL_HOST = 'smtp.zoho.eu'
    EMAIL_HOST_USER = 'maticnjak@zohomail.eu'
    EMAIL_HOST_PASSWORD = 'Uytspo9tc'
    EMAIL_PORT = 587
    DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
    SERVER_EMAIL = DEFAULT_FROM_EMAIL
    EMAIL_SUBJECT_PREFIX = '[Maticnjak] '
    EMAIL_BACKEND = 'post_office.EmailBackend'

    # Internationalization
    LANGUAGE_CODE = 'hr'
    LANGUAGES = [
        ('hr', _('Hrvatski')),
        # ('en', _('English')),
    ]
    TIME_ZONE = 'Europe/Zagreb'
    USE_I18N = True
    USE_L10N = True
    # USE_TZ = True
    USE_TZ = True
    LOCALE_PATHS = values.ListValue([os.path.join(BASE_DIR, 'locale')])

    # parler
    PARLER_LANGUAGES = {1: tuple({'code': x[0]} for x in LANGUAGES)}

    # Static & media files
    STATIC_ROOT = values.PathValue()
    MEDIA_ROOT = values.PathValue()
    STATIC_URL = '/static/'
    MEDIA_URL = '/media/'
    STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]
    STATICFILES_FINDERS = [
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        'compressor.finders.CompressorFinder',
    ]

    # webpack_loader
    WEBPACK_LOADER = {
        'DEFAULT': {
            'BUNDLE_DIR_NAME': 'bundles/',
            'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats.json'),
        }
    }

    # compressor
    COMPRESS_STORAGE = 'compressor.storage.GzipCompressorFileStorage'
    COMPRESS_JS_FILTERS = []
    COMPRESS_CSS_FILTERS = [
        'compressor.filters.css_default.CssAbsoluteFilter',
        'compressor.filters.cssmin.CSSMinFilter',
    ]

    # htmlmin
    EXCLUDE_FROM_MINIFYING = [r'^admin/', r'^translate/']

    # rosetta
    ROSETTA_SHOW_AT_ADMIN_PANEL = False

    # admin_shortcuts
    ADMIN_SHORTCUTS_SETTINGS = {
        'hide_app_list': False,
        'open_new_window': False,
        'show_on_all_pages': True,
    }
    ADMIN_SHORTCUTS = [
        {'title': _('Basic'), 'shortcuts': [
            {'url': '/', 'open_new_window': True},
            {'url_name': 'admin:cms_page_changelist', 'title': _('Pages')},
            {'url_name': 'admin:filer_folder_changelist', 'title': _('Files')},
        ]},

        {'title': _('News'), 'shortcuts': [
            {'url_name': 'admin:blogit_post_changelist', 'title': _('Posts')},
            {'url_name': 'admin:blogit_category_changelist', 'title': _('Categories')},
            {'url_name': 'admin:blogit_tag_changelist', 'title': _('Tags')},
        ]},
        {'title': _('Shop'), 'shortcuts': [
            {'url_name': 'admin:shopit_product_changelist', 'title': _('Products')},
            {'url_name': 'admin:shopit_category_changelist', 'title': _('Categories')},
            {'url_name': 'admin:shopit_order_changelist', 'title': _('Orders'),
             'count_new': 'shopit_extra.admin_shortcuts.count_new_orders',
             'url_extra': '?status__in=awaiting_payment,payment_confirmed,awaiting_payment_on_delivery,paid_with_wspay'},  # noqa
        ]},
        {'title': _('Recipes'), 'shortcuts': [
            {'url_name': 'admin:recipes_recipe_changelist', 'title': _('Recipes')},
        ]},
        {'title': _('Media'), 'shortcuts': [
            {'url_name': 'admin:media_media_changelist', 'title': _('Media')},
        ]},
        {'title': _('Testimonials'), 'shortcuts': [
            {'url_name': 'admin:testimonials_testimonial_changelist', 'title': _('Testimonials')},
        ]},
    ]

    ADMIN_SHORTCUTS_CLASS_MAPPINGS = [
        ['tag', 'tag'],
        ['product', 'basket'],
        ['post', 'letter'],
        ['cms_page', 'file2'],
        ['order', 'cash'],
        ['category', 'archive'],
        ['user', 'user'],
        ['account', 'user'],
        ['address', 'letter'],
        ['folder', 'folder'],
        ['home', 'home'],
        ['recipes_recipe', 'letter'],
        ['media_media', 'tag'],
        ['testimonials_testimonial', 'user'],
    ]

    # cms
    CMS_SEO_FIELDS = True
    CMS_HIDE_UNTRANSLATED = True
    CMS_REDIRECTS = True
    CMS_TEMPLATES = [
        ('default.html', _('Default')),
        ('apitherapy.html', _('Apitherapy')),
        ('phytotherapy.html', _('Phytotherapy')),
        ('about.html', _('About')),
        ('locations.html', _('Locations')),
        ('contact.html', _('Kontakt')),
        ('dealership.html', _('Dealership')),
        ('home.html', _('Home')),

        ('shopit/account/account_order_detail.html', _('Order detail')),
        ('shopit/shop/thanks.html', _('Thanks')),
    ]
    CMS_PLACEHOLDER_CONF = {
        'content': {
            'plugins': [
                'TextPlugin',
                'LinkPlugin',
                'PicturePlugin',
                'FilePlugin',
                'VideoPlayerPlugin',
                'GalleryPlugin',
                'QuotePlugin',
                'ImageWithContentPlugin',
            ],
            'name': _('Content'),
        },
        'apitherapy': {
            'plugins': ['ImageWithContentPlugin'],
            'name': _('Apitherapy content'),
        },
        'phytotherapy': {
            'plugins': ['ImageWithContentPlugin', 'FactsPlugin'],
            'name': _('Phytotherapy content'),
        },
        'about': {
            'plugins': ['ImageWithContentPlugin', 'FactsPlugin'],
            'name': _('About content'),
        },
        'locations': {
            'plugins': ['WebShopsPlugin', 'StoresPlugin'],
            'name': _('Locations content'),
        },
        'recipes_recipe_content': {
            'plugins': ['TextPlugin'],
            'name': _('Content'),
        },
        'blogit_post_body': {
            'inherit': 'content',
            'name': _('Post content'),
        },
        'shopit_product_content': {
            'plugins': [
                'TextPlugin',
                'LinkPlugin',
                'PicturePlugin',
                'FilePlugin',
                'VideoPlayerPlugin',
                'GalleryPlugin',
                'QuotePlugin',
                'ImageWithContentPlugin',
                'ProductSetPlugin',
                'FeaturedProductsPlugin',
            ],
            'name': _('Content'),
        },

        'home_content': {
            'plugins': ['ProductSetPlugin'],
            'name': _('Home Content'),
        },
    }

    # easy_thumbnails
    THUMBNAIL_HIGH_RESOLUTION = True
    THUMBNAIL_PROCESSORS = (
        'easy_thumbnails.processors.colorspace',
        'easy_thumbnails.processors.autocrop',
        'filer.thumbnail_processors.scale_and_crop_with_subject_location',
        'easy_thumbnails.processors.filters',
    )
    THUMBNAIL_ALIASES = {
        'shopit.Attachment': {
            'thumb': {'size': (200, 300), 'crop': False, 'upscale':  False},
            'cart': {'size': (92, 105), 'crop':  False, 'upscale':  False},
        },
    }
    CMS_ENABLE_UPDATE_CHECK = False

    # filer
    FILER_ALLOW_REGULAR_USERS_TO_ADD_ROOT_FOLDERS = True

   # djangocms_text_ckeditor
    CKEDITOR_TOOLBAR = [
        ['Undo', 'Redo'],
        ['cmsplugins', '-', 'ShowBlocks'],
        ['Format', 'Styles'],
        ['PasteText', 'PasteFromWord'],
        ['Maximize', ''],
        '/',
        ['Bold', 'Italic', 'Underline', '-', 'RemoveFormat'],
        ['Link', 'Unlink'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
        ['Table', 'HorizontalRule'],
        ['NumberedList', 'BulletedList', '-'],
        ['Source']
    ]
    CKEDITOR_SETTINGS = {
        'language': 'en',
        'toolbar_CMS': CKEDITOR_TOOLBAR,
        'toolbar_HTMLField': CKEDITOR_TOOLBAR,
        'contentsCss': ['/static/bundles/style.css'],
        'skin': 'moono',
    }
    TEXT_SAVE_IMAGE_FUNCTION = 'djangocms_text_ckeditor.picture_save.create_picture_plugin'

    # haystack
    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.elasticsearch2_backend.Elasticsearch2SearchEngine',
            'URL': 'http://127.0.0.1:9200/',
            'INDEX_NAME': 'maticnjak',
        },
    }

    HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
    HAYSTACK_SEARCH_RESULTS_PER_PAGE = 8

    # rest_framework
    REST_FRAMEWORK = {
        'DEFAULT_RENDERER_CLASSES': [
            'shop.rest.money.JSONRenderer',
            # 'rest_framework.renderers.BrowsableAPIRenderer',
        ],
        'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
        'PAGE_SIZE': 12,
    }
    SERIALIZATION_MODULES = {'json': 'shop.money.serializers'}
    COERCE_DECIMAL_TO_STRING = True

    # shop, shopit
    SHOP_APP_LABEL = 'shopit'
    SHOP_DEFAULT_CURRENCY = 'HRK'
    SHOP_VENDOR_EMAIL = 'prodaja@maticnjak.hr'  # not real
    USE_THOUSAND_SEPARATOR = True
    SHOP_MONEY_FORMAT = '{minus}{amount} {symbol}'
    SHOP_PRODUCT_SUMMARY_SERIALIZER = 'shopit.serializers.ProductSummarySerializer'
    SHOP_CART_MODIFIERS = [
        'shop.modifiers.defaults.DefaultCartModifier',
        'shopit.modifiers.ShopitCartModifier',

        'shopit_extra.modifiers.PayOnDeliveryModifier',
        'shop_wspay.modifiers.WSPayPaymentModifier',

        'shopit_extra.modifiers.GLSShippingModifier',
    ]
    SHOP_ORDER_WORKFLOWS = [
        'shopit_extra.payment.ModifiedWSPayWorkflowMixin',
        'shopit_extra.payment.PayOnDeliveryWorkflowMixin',
        'shopit_extra.payment.CancelOrderWorkflowMixin',

        'shopit.shipping.ShippingWorkflowMixin',
    ]
    SHOP_GUEST_IS_ACTIVE_USER = False
    SHOP_CART_URL = 'shopit-cart'
    SHOP_THANK_YOU_URL = 'shopit-thanks'

    SHOPIT_PHONE_NUMBER_REQUIRED = True
    SHOPIT_PRIMARY_ADDRESS = 'billing'
    SHOPIT_ASYNC_PRODUCT_LIST = True
    SHOPIT_FILTER_ATTRIBUTES_INCLUDES_VARIANTS = False
    SHOPIT_PRODUCT_SERIALIZER_FIELDS = [
        'id', 'name', 'slug', 'caption', 'code', 'kind', 'url','order', 'add_to_cart_url', 'price', 'is_available',
        'attachments', 'discount_percent', 'is_discounted', 'unit_price', 'flags', 'is_single', 'is_group', 'quantity'
    ]
    SHOPIT_DEFAULT_PRODUCT_ORDER = 'price'
    SHOPIT_PRODUCT_DETAIL_SERIALIZER_FIELDS = SHOPIT_PRODUCT_SERIALIZER_FIELDS + [
        'variants', 'attributes', 'attribute_choices', 'description'
    ]
    # SHOPIT_ATTRIBUTE_TEMPLATES = [
    #     ('', '---------'),
    #     ('color', _('Color')),
    #     ('size', _('Size')),
    # ]
    # SHOPIT_FLAG_TEMPLATES = [
    #     ('', '---------'),
    #     ('orange', _('Orange')),
    #     ('red', _('Red')),
    #     ('green', _('Green')),
    #     ('blue', _('Blue')),
    # ]

    from .address_countries import ADDRESS_COUNTRIES_ZONES
    SHOPIT_ADDRESS_COUNTRIES = [(x[0], x[1]) for x in ADDRESS_COUNTRIES_ZONES]

    # shop_wspay
    SHOP_WSPAY_SHOP_ID = 'MATICNJAK'
    SHOP_WSPAY_SECRET_KEY = '8d5a6cd9def24T'
    SHOP_WSPAY_MODIFIER_CHOICE_TEXT = _('Kartično plaćanje (WSPay)')
    # Production url.
    SHOP_WSPAY_FORM_URL = 'https://form.wspay.biz/Authorization.aspx'
    # Testing url.
    # SHOP_WSPAY_FORM_URL = 'https://formtest.wspay.biz/Authorization.aspx'

    # phonenumber_field
    # PHONENUMBER_DB_FORMAT = 'INTERNATIONAL'
    # PHONENUMBER_DEFAULT_FORMAT = 'INTERNATIONAL'

    # blogit
    BLOGIT_POSTS_PER_PAGE = 18


class Development(Common):
    """
    The in-development settings
    """
    DEBUG = values.BooleanValue(True)
    # SECRET_KEY = 'secret'
    ALLOWED_HOSTS = ['*']
    INTERNAL_IPS = ['127.0.0.1']

    # EMAIL = values.EmailURLValue('console://')
    EMAIL = 'post_office.EmailBackend'

    # INSTALLED_APPS = Common.INSTALLED_APPS + ['django_extensions', 'sslserver']
    INSTALLED_APPS = Common.INSTALLED_APPS + ['django_extensions']

    # MIDDLEWARE = Common.MIDDLEWARE + ['querycount.middleware.QueryCountMiddleware']

    # Static files
    MEDIA_ROOT = values.PathValue(os.path.join(BASE_DIR, 'public/media'), check_exists=False)
    STATIC_ROOT = values.PathValue(os.path.join(BASE_DIR, 'public/static'), check_exists=False)

    # debug_toolbar
    DEBUG_TOOLBAR = values.BooleanValue(DEBUG)

    COMPRESS_ENABLED = False
    HTML_MINIFY = False

    @property
    def WEBPACK_LOADER(self):
        # Set webpack dev stats file
        webpack = dict(Common.WEBPACK_LOADER)
        webpack['DEFAULT']['STATS_FILE'] = os.path.join(BASE_DIR, 'webpack-stats-dev.json')
        return webpack

    @classmethod
    def setup(cls):
        super(Development, cls).setup()
        # Activate django-debug-toolbar
        if cls.DEBUG_TOOLBAR:
            cls.INSTALLED_APPS += ['debug_toolbar']
            cls.MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']


class Staging(Common):
    """
    The in-staging settings
    """
    DEBUG = values.BooleanValue(False)
    ALLOWED_HOSTS = ['*']
    # ALLOWED_HOSTS = values.ListValue(['.maticnjakshop.com', '161.35.211.179', '127.0.0.1', 'localhost', 'maticnjak.dikobraz.hr'])

    # MIDDLEWARE = Common.MIDDLEWARE + ['utils.demo_middleware.DemoMiddleware']

    SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

    COMPRESS_ENABLED = True
    HTML_MINIFY = False

    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.elasticsearch2_backend.Elasticsearch2SearchEngine',
            'URL': 'http://elasticsearch:9200/',
            'INDEX_NAME': 'web',
        },
    }


class Production(Staging):
    """
    The in-production settings
    """
    # Security
    SESSION_COOKIE_SECURE = values.BooleanValue(True)
    SECURE_BROWSER_XSS_FILTER = values.BooleanValue(True)
    SECURE_CONTENT_TYPE_NOSNIFF = values.BooleanValue(True)
    SECURE_HSTS_INCLUDE_SUBDOMAINS = values.BooleanValue(True)
    SECURE_HSTS_SECONDS = values.IntegerValue(31536000)
    SECURE_REDIRECT_EXEMPT = values.ListValue([])
    SECURE_SSL_HOST = values.Value(None)
    SECURE_SSL_REDIRECT = values.BooleanValue(True)
    SECURE_PROXY_SSL_HEADER = values.TupleValue(('HTTP_X_FORWARDED_PROTO', 'https'))
