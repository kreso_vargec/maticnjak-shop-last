# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView
from django.views.decorators.clickjacking import xframe_options_exempt
from django.shortcuts import render
from rest_framework import routers
from shopit_extra.sitemaps import ProductSitemap

from search.views import SearchViewSet

sitemaps = {
    'cmspages': CMSSitemap,
}

router = routers.DefaultRouter()
router.register(r'search', SearchViewSet, base_name='search')

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}),
    url(r'^api/', include(router.urls, namespace='api')),

    url(r'^shop/', include('shopit_extra.urls', namespace='shop')),
    url(r'^contact/', include('contact.urls')),
    url(r'^dealership/', include('dealership.urls')),

    url(r'^demo/$', TemplateView.as_view(template_name='demo.html')),
    url(r'^social/', include('social_django.urls', namespace='social')),
]

# Handle static & media files in dev.
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# Add django-debug-toolbar urls.
if getattr(settings, 'DEBUG_TOOLBAR', False):
    import debug_toolbar
    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]


urlpatterns += i18n_patterns(
    url(r'^admin/', include(admin.site.urls)),
    url(r'^translate/', include('rosetta.urls')),
    url(r'^', include('cms.urls')),
    prefix_default_language=False,
)
