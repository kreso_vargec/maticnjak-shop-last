# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import NoReverseMatch
from django.template import Library
from django.utils import timezone
from django.utils.formats import date_format
from django.core.urlresolvers import reverse
from blogit.models import Category, Post

register = Library()


def _menu_generator(request, items):
    for item in items:
        try:
            url = item.get_absolute_url()
            yield {
                'name': str(item),
                'url': url,
                'active': request.path.endswith(url)
            }
        except NoReverseMatch:
            pass


@register.simple_tag(takes_context=True)
def get_blogit_category_menu(context):
    categories = Category.objects.translated().filter(level=0)
    return _menu_generator(context['request'], categories)


@register.simple_tag
def get_blog_category_by_id(id):
    try:
        return Category.objects.get(id=id)
    except Category.DoesNotExist:
        pass


@register.simple_tag
def get_dates(limit=None):
    return Post.objects.dates('date_published', 'year')[:limit]


@register.simple_tag(takes_context=True)
def get_other_posts(context, post, limit=2):
    request = context['request']
    other = Post.objects.published(request).exclude(id=post.id)
    if post.category:
        in_cat = other.filter(category=post.category)
        if in_cat.count():
            other = in_cat
    return other[:limit]


@register.simple_tag
def get_random_category(limit=None):
    return Category.objects.all().order_by('?')[:limit]


@register.simple_tag
def get_latest_news(limit=2, category=None):
    posts = Post.objects.public().translated()
    if category:
        posts = posts.filter(category=category)
    return posts[:limit]





@register.simple_tag(takes_context=True)
def get_archive_date(context):
    if 'day' in context:
        return context['day']
    if 'month' in context:
        return context['month']
    if 'year' in context:
        return context['year']


@register.simple_tag(takes_context=True)
def get_archive_filters(context):
    current = get_archive_date(context)

    years = []
    year_dates = Post.objects.public().filter().values_list('date_published', flat=True)
    for year in year_dates:
        year_value = date_format(year, 'Y')
        year = dict(
            name=year_value,
            value=year_value,
            url=reverse('blogit_post_archive_year', args=[year_value]),
        )
        if year not in years:
            years.append(year)

    months = []
    month_dates = Post.objects.public().filter(date_published__year=current.year).\
        values_list('date_published', flat=True)

    for month in month_dates:
        month_value = date_format(month, 'm')
        month = dict(
            name=date_format(month, 'F'),
            value=month_value,
            url=reverse('blogit_post_archive_month', args=[current.year, month_value]),
        )
        if month not in months:
            months.append(month)

    return dict(
        months=sorted(months, key=lambda x: x['value']),
        years=reversed(sorted(years, key=lambda x: x['value'])),
    )
