# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from djangocms_text_ckeditor.fields import HTMLField

from cms.models import CMSPlugin
from filer.fields.image import FilerImageField


@python_2_unicode_compatible
class ImageWithContent(CMSPlugin):
    TEMPLATES = [
        ('even', _('Image left, text right')),
        ('even-extra', _('Image left with flower, text right')),
        ('odd', _('Image right, text left')),
        ('odd-extra', _('Image right with flower, text left')),
    ]

    template = models.CharField(
        verbose_name=_('Template'),
        choices=TEMPLATES,
        default=TEMPLATES[0][0],
        max_length=255,
    )

    image = FilerImageField(
        verbose_name=_('Image'),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='+',
    )

    content = HTMLField(
        blank=True,
        verbose_name=_('Content'),
    )

    phytotherapy_button = models.BooleanField(
        verbose_name=_('Phytotherapy button'),
        default=False,
        help_text=_('Show phytotherapy button?')
    )

    apitherapy_button = models.BooleanField(
        verbose_name=_('Apitherapy Button'),
        default=False,
        help_text=_('Show phytotherapy button?')
    )

    def __str__(self):
        return str(self.pk)
