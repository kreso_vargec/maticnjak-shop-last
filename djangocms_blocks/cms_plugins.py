# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import ImageWithContent


@plugin_pool.register_plugin
class ImageWithContentPlugin(CMSPluginBase):
    model = ImageWithContent
    name = _('Image with Content')
    render_template = 'djangocms_blocks/image_with_content.html'
