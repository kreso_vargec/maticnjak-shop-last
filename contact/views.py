# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from .serializers import ContactFormSerializer


class ContactFormView(APIView):
    renderer_classes = [JSONRenderer]

    def post(self, request, format=None):
        serializer = ContactFormSerializer(data=request.data)
        if serializer.is_valid():
            if serializer.send_mail(request):
                return Response({'success': _('Message successfully sent!')})
            return Response({'non_field_errors': [_('There was an error sending the email!')]}, status=400)
        return Response(serializer.errors, status=400)
