# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import smtplib

from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from post_office import mail

from rest_framework import serializers
from cms.models import Page

from utils.templatetags.utils_url import get_absolute_url_with_fallback


class ContactFormSerializer(serializers.Serializer):
    LABELS = {
        'name': _('Vaše ime i prezime'),
        'email': _('Vaša e-mail adresa'),
        'phone': _('Vaš broj telefona'),
        'message': _('Vaša poruka'),
        'accept': _('Prihvaćam <a href="%s">Politiku privatnosti</a>.'),
    }

    name = serializers.CharField(label=LABELS['name'])
    email = serializers.EmailField(label=LABELS['email'])
    phone = serializers.CharField(label=LABELS['phone'], required=False)
    message = serializers.CharField(label=LABELS['message'])
    accept = serializers.BooleanField(label=LABELS['accept'], required=True)

    def __init__(self, *args, **kwargs):
        super(ContactFormSerializer, self).__init__(*args, **kwargs)
        terms_page = Page.objects.filter(reverse_id='terms_and_conditions').first()
        if terms_page:
            self.fields['accept'].label = self.LABELS['accept'] % get_absolute_url_with_fallback(terms_page)

    def send_mail(self, request):
        subject = '%s%s' % (settings.EMAIL_SUBJECT_PREFIX, _('Kontakt'))


        body = ''
        for field, label in self.LABELS.items():
            if field == 'accept':
                continue
            value = self.data.get(field, '-')
            body += '{0}:\n{1}\n\n'.format(label, value)

        message = mail.send(
            recipients=[x[1] for x in settings.MANAGERS],
            subject=subject,
            message=body,
            headers={'Reply-to': self.data['email']},
            context=self.data,
        )

        try:
            return bool(message)
        except smtplib.SMTPException:
            return False
