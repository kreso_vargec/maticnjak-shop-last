# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from .views import ContactFormView


urlpatterns = [
    url('form/', ContactFormView.as_view(), name='contact-form'),
]
