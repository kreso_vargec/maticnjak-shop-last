const ip = require('ip')
const path = require('path')
const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const BundleTracker = require('webpack-bundle-tracker')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  context: __dirname,
  entry: [
    '@babel/polyfill',

    // Needed for Plyr.
    'custom-event-polyfill',
    'url-polyfill',

    './assets/js/main.js'
  ],
  output: {
    path: path.resolve('./static/bundles/'),
    filename: "[name].js"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!postcss-loader'
        }))
      },
      {
        test: /\.scss$/,
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!postcss-loader!sass-loader'
        }))
      },
      {
        test: /\.sass$/,
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!postcss-loader!sass-loader?indentedSyntax'
        }))
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            css: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
              fallback: 'vue-style-loader',
              use: 'css-loader'
            })),
            scss: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
              fallback: 'vue-style-loader',
              use: 'css-loader!sass-loader'
            })),
            sass: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
              fallback: 'vue-style-loader',
              use: 'css-loader!sass-loader?indentedSyntax'
            }))
          }
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'images/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'media/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'fonts/[name].[hash:7].[ext]'
        }
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('style.css')
  ],
  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js',
      styles: path.resolve(__dirname, 'assets/sass'),

      'TweenLite': 'gsap/src/uncompressed/TweenLite.js',
      'TweenMax': 'gsap/src/uncompressed/TweenMax.js',
      'TimelineLite': 'gsap/src/uncompressed/TimelineLite.js',
      'TimelineMax': 'gsap/src/uncompressed/TimelineMax.js',
      'EasePack': 'gsap/src/uncompressed/easing/EasePack.js',
      'ScrollMagic': 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
      'animation.gsap': 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js',
      'debug.addIndicators': 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js',
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    host: '0.0.0.0',
    port: 3000,
    https: false,
    disableHostCheck: true,
    headers: {'Access-Control-Allow-Origin': '*'}
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'development') {
  module.exports.output.publicPath = `http://${ip.address()}:3000/static/bundles/`
  module.exports.plugins = (module.exports.plugins || []).concat([
    new BundleTracker({filename: './webpack-stats-dev.json'})
  ])
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  module.exports.output.publicPath = '/static/bundles/'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new BundleTracker({filename: './webpack-stats.json'}),
    new webpack.DefinePlugin({'process.env': {NODE_ENV: '"production"'}}),
    new CleanWebpackPlugin(['./static/bundles']),
    new UglifyJsPlugin({sourceMap: true}),
    new webpack.LoaderOptionsPlugin({minimize: true})
  ])
}
