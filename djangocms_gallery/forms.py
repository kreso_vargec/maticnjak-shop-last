# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Gallery, GalleryImage


class GalleryForm(forms.ModelForm):
    class Meta:
        model = Gallery
        exclude = []

    def clean(self):
        cleaned_data = super(GalleryForm, self).clean()
        generate_image_plugins = cleaned_data.get('generate_image_plugins')
        folder = cleaned_data.get('folder')

        if generate_image_plugins and not folder:
            self.add_error('generate_image_plugins', _('To generate image plugins you must select a folder.'))


class GalleryImageForm(forms.ModelForm):
    class Meta:
        model = GalleryImage
        exclude = []
        widgets = {'caption': forms.Textarea(attrs={'rows': 2})}
