# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from cms.models import CMSPlugin
from cms.api import add_plugin

from filer.fields.folder import FilerFolderField
from filer.fields.image import FilerImageField


# Add additional choices through the ``settings.py``.
def get_templates():
    choices = [
        ('default', _('Default')),
        ('slider', _('Slider')),
    ]
    choices += getattr(settings, 'DJANGOCMS_GALLERY_TEMPLATES', [])
    return choices


@python_2_unicode_compatible
class Gallery(CMSPlugin):
    """
    Gallery plugin model.
    """
    template = models.CharField(
        verbose_name=_('Template'),
        choices=get_templates(),
        default=get_templates()[0][0],
        max_length=255,
    )

    folder = FilerFolderField(
        verbose_name=_('Folder'),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text=_('Select a folder or leave empty to populate images as child plugins.')
    )

    generate_image_plugins = models.BooleanField(
        verbose_name=_('Generate image plugins'),
        default=False,
        help_text=_(
            'If checked, upon saving, an image plugin will be created '
            'for every image inside the selected folder while skipping the existing ones.'
        )
    )

    def __str__(self):
        if self.folder:
            return _('Folder: %(name)s') % {'name': self.folder.name}
        return str(self.pk)

    def save(self, *args, **kwargs):
        if self.generate_image_plugins and self.folder:
            existing_image_ids = GalleryImage.objects.filter(parent=self).values_list('image__id', flat=True)
            folder_images = self.get_folder_images()
            self.folder = None
            self.generate_image_plugins = False
            super(Gallery, self).save(*args, **kwargs)
            for image in folder_images:
                if image.id not in existing_image_ids:
                    add_plugin(
                        self.placeholder,
                        'GalleryImagePlugin',
                        self.language,
                        position='last-child',
                        target=self,
                        image=image,
                    )
        else:
            self.generate_image_plugins = False
            super(Gallery, self).save(*args, **kwargs)

    def copy_relations(self, oldinstance):
        self.folder = oldinstance.folder

    def get_folder_images(self):
        folder_images = []
        if self.folder:
            for file in self.folder.files:
                if file.file_type == 'Image':
                    folder_images.append(file)
        return folder_images


@python_2_unicode_compatible
class GalleryImage(CMSPlugin):
    """
    Gallery image plugin model.
    """
    image = FilerImageField(
        verbose_name=_('Image'),
        on_delete=models.SET_NULL,
        null=True,
        related_name='+',
    )

    caption = models.TextField(
        verbose_name=_('Caption'),
        blank=True,
    )

    def __str__(self):
        return str(self.pk)


    @property
    def file(self):
        return self.image

    @property
    def description(self):
        return self.caption

    @property
    def height(self):
        return self.image.height

    @property
    def width(self):
        return self.image.width
