# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import Gallery, GalleryImage
from .forms import GalleryForm, GalleryImageForm


@plugin_pool.register_plugin
class GalleryPlugin(CMSPluginBase):
    model = Gallery
    form = GalleryForm
    name = _('Gallery')
    allow_children = True
    child_classes = ['GalleryImagePlugin']

    fieldsets = [
        (None, {'fields': ['folder', 'generate_image_plugins']}),
        (_('Advanced settings'), {
            'classes': ['collapse'],
            'fields': ['template'],
        }),
    ]

    def get_render_template(self, context, instance, placeholder):
        return 'djangocms_gallery/{}/gallery.html'.format(instance.template)

    def render(self, context, instance, placeholder):
        context = super(GalleryPlugin, self).render(context, instance, placeholder)
        context['gallery_id'] = instance.id
        context['gallery_template'] = instance.template
        context['gallery_folder_images'] = instance.get_folder_images()
        return context


@plugin_pool.register_plugin
class GalleryImagePlugin(CMSPluginBase):
    model = GalleryImage
    form = GalleryImageForm
    name = _('Image')
    module = _('Gallery')
    require_parent = True
    parent_classes = ['GalleryPlugin']

    fieldsets = [
        (None, {'fields': ['image', 'caption']}),
    ]

    def get_render_template(self, context, instance, placeholder):
        return 'djangocms_gallery/{}/image.html'.format(context['gallery_template'])
