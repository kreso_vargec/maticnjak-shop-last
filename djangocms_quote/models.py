# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import Truncator

from cms.models import CMSPlugin


def get_templates():
    choices = [('default', _('Default'))]
    choices += getattr(settings, 'DJANGOCMS_QUOTE_TEMPLATES', [])
    return choices


@python_2_unicode_compatible
class Quote(CMSPlugin):
    """
    Quote model.
    """
    template = models.CharField(
        verbose_name=_('Template'),
        choices=get_templates(),
        default=get_templates()[0][0],
        max_length=255,
    )

    title = models.CharField(
        verbose_name=_('Title'),
        max_length=255,
        blank=True,
        help_text=_('An optional quote title.'),
    )

    text = models.TextField(
        verbose_name=_('Text'),
        help_text=_('Main content of the quote.'),
    )

    author = models.CharField(
        verbose_name=_('Author'),
        max_length=255,
        blank=True,
        help_text=_('Author name.'),
    )

    # Add an app namespace to related_name to avoid field name clashes
    # with any other plugins that have a field with the same name as the
    # lowercase of the class name of this model.
    # https://github.com/divio/django-cms/issues/5030
    cmsplugin_ptr = models.OneToOneField(
        CMSPlugin,
        related_name='%(app_label)s_%(class)s',
        parent_link=True,
    )

    def __str__(self):
        return self.title or Truncator(self.text).words(3)
