# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import Quote


class QuotePlugin(CMSPluginBase):
    model = Quote
    name = _('Quote')

    fieldsets = [
        (None, {'fields': ['text', 'author']}),
        (_('Advanced settings'), {
            'classes': ['collapse'],
            'fields': ['template'],
        }),
    ]

    def get_render_template(self, context, instance, placeholder):
        return 'djangocms_quote/{}/quote.html'.format(instance.template)


plugin_pool.register_plugin(QuotePlugin)
