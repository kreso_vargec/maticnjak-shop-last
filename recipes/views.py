# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import ListView, DeleteView
from parler.views import TranslatableSlugMixin

from recipes.models import Recipe


class RecipeListView(ListView):
    model = Recipe
    template_name = 'recipes/recipe_list.html'
    paginate_by = 100

    def get_queryset(self):
        return self.model.objects.all()


class RecipeDetailView(TranslatableSlugMixin, DeleteView):
    model = Recipe
    template_name = 'recipes/recipe_detail.html'

    def get_view_url(self):
        return self.object.get_absolute_url()
