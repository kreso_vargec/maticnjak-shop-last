# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from recipes.views import RecipeListView, RecipeDetailView


class RecipesApphook(CMSApp):
    name = _('Recipes')

    def get_urls(self, page=None, language=None, **kwargs):
        return [
            url(r'^$', RecipeListView.as_view()),
            url(r'^(?P<slug>[-_\w\d]+)/$', RecipeDetailView.as_view(), name='recipes_recipe_detail'),
        ]

apphook_pool.register(RecipesApphook)
