from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.toolbar.items import Break, SubMenu
from django.core.urlresolvers import reverse

from recipes.models import Recipe

try:
    from cms.cms_toolbars import ADMIN_MENU_IDENTIFIER, ADMINISTRATION_BREAK
except ImportError:
    from cms.cms_toolbar import ADMIN_MENU_IDENTIFIER, ADMINISTRATION_BREAK


class RecipeToolbar(CMSToolbar):
    supported_apps = (
        'recipes',
    )

    watch_models = [Recipe]

    def populate(self):
        admin_menu = self.toolbar.get_menu(ADMIN_MENU_IDENTIFIER)

        if admin_menu:
            position = admin_menu.get_alphabetical_insert_position(_('Recipes'), SubMenu)
            if not position:
                position = admin_menu.find_first(Break, identifier=ADMINISTRATION_BREAK) + 1
                admin_menu.add_break('recipe-break', position=position)

            menu = admin_menu.get_or_create_menu('recipes-admin-menu', _('Recipes'), position=position)

            menu.add_sideframe_item(_('Recipes list'), url=reverse('admin:recipes_recipe_changelist'))
            menu.add_modal_item(_('Add new recipe'), url=reverse('admin:recipes_recipe_add'))
            menu.add_break()

        if self.is_current_app:
            current_menu = self.toolbar.get_or_create_menu('recipes-current-menu', _('Recipes'))
            add_menu = current_menu.get_or_create_menu('recipes-current-menu-add', _('Add New'))
            add_menu.add_modal_item(_('Recipes'), url=reverse('admin:recipes_recipe_add'))

toolbar_pool.register(RecipeToolbar)
