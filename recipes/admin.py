from django.contrib import admin
from parler.admin import TranslatableAdmin
from cms.admin.placeholderadmin import FrontendEditableAdminMixin, PlaceholderAdminMixin
from adminsortable2.admin import SortableAdminMixin
from recipes.models import Recipe


@admin.register(Recipe)
class RecipeAdmin(SortableAdminMixin, FrontendEditableAdminMixin, PlaceholderAdminMixin, TranslatableAdmin):
    frontend_editable_fields = [
        'name',
    ]

    list_display = ['name']

    readonly_fields = ['created', 'modified']

    fieldsets = [
        (None, {'fields': [('name', 'slug'), 'featured_image']}),
        (None, {'fields': [('created', 'modified')]})
    ]

    def get_prepopulated_fields(self, request, object=None):
        return {'slug': ['name']}
