# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template
from recipes.models import Recipe

register = template.Library()


@register.simple_tag
def get_recipies(limit=None):
    return Recipe.objects.all()[:limit]
