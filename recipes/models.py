from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, NoReverseMatch
from parler.models import TranslatableModel, TranslatedFields
from parler.utils.context import switch_language
from filer.fields.image import FilerImageField
from cms.utils.i18n import get_current_language
from cms.models.fields import PlaceholderField


@python_2_unicode_compatible
class Recipe(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(_('Name'), max_length=128),
        slug=models.SlugField(_('Slug'), db_index=True),
        meta={'unique_together': [('slug', 'language_code')]},
    )

    featured_image = FilerImageField(
        verbose_name=_('Featured image'),
        related_name='+',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    content = PlaceholderField('recipes_recipe_content')

    created = models.DateTimeField(
        verbose_name=_('Created'),
        auto_now_add=True
    )

    modified = models.DateTimeField(
        verbose_name=_('Modified'),
        auto_now=True
    )

    order = models.PositiveIntegerField(
        verbose_name=_('Sort'),
        default=0
    )

    class Meta:
        verbose_name = _('Recipe')
        verbose_name_plural = _('Recipies')
        ordering = ['order']

    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)

    def get_absolute_url(self, language=None):
        if not language:
            language = get_current_language()

        with switch_language(self, language):
            slug = self.safe_translation_getter('slug')
            try:
                return reverse('recipes_recipe_detail', args=[slug])
            except NoReverseMatch:
                pass

    @property
    def previous_recipe(self):
        return self.previous_next_recipes[0]

    @property
    def next_recipe(self):
        return self.previous_next_recipes[1]

    @property
    def previous_next_recipes(self):
        previous_next = getattr(self, 'previous_next', None)

        if previous_next is None:
            recipes = list(Recipe.objects.all())
            index = recipes.index(self)

            try:
                previous = recipes[index + 1]
            except IndexError:
                previous = None

            if index:
                next = recipes[index - 1]
            else:
                next = None
            previous_next = (previous, next)
            setattr(self, 'previous_next', previous_next)
        return previous_next
