#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import time

from dotenv import load_dotenv
from gevent import monkey
import pymysql

load_dotenv(override=True)
monkey.patch_all()
pymysql.install_as_MySQLdb()

import schedule  # noqa


if __name__ == '__main__':
    settings = os.getenv('ENVIRONMENT', 'production')
    assert settings in ['staging', 'production', 'development', 'test'], \
        "ENVIRONMENT variable must be set to either staging, production, development or test."

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')
    os.environ.setdefault('DJANGO_CONFIGURATION', settings.title())

    import configurations
    configurations.setup()

    from configurations.management import call_command
    from django.utils import timezone

    # Mail
    schedule.every().minute.do(call_command, 'send_queued_mail', processes=1)
    schedule.every().day.at("08:00").do(call_command, 'cleanup_mail', days=30)

    # Indexes
    rebuild_at = timezone.now() + timezone.timedelta(minutes=6)
    schedule.every().hour.at(rebuild_at.strftime('*:%M')).do(call_command, 'update_index')
    schedule.every().day.at("09:00").do(call_command, 'rebuild_index', interactive=False)

    # Shop
    schedule.every().sunday.do(call_command, 'shopcustomers', delete_expired=True)

    while True:
        schedule.run_pending()
        time.sleep(1)
