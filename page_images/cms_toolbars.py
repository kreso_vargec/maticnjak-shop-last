# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from cms.toolbar_pool import toolbar_pool
from cms.extensions.toolbar import ExtensionToolbar

from page_images.models import PageImagesExtension


@toolbar_pool.register
class PageImagesExtensionToolbar(ExtensionToolbar):
    model = PageImagesExtension

    def populate(self):
        current_page_menu = self._setup_extension_toolbar()
        if current_page_menu:
            page_extension, url = self.get_page_extension_admin()
            if url:
                current_page_menu.add_modal_item(
                    _('Page images'),
                    url=url,
                    disabled=not self.toolbar.edit_mode_active,
                    position=4,
                )
