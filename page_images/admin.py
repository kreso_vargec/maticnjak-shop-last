# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from cms.extensions import PageExtensionAdmin

from page_images.models import PageImagesExtension


@admin.register(PageImagesExtension)
class PageImagesExtensionAdmin(PageExtensionAdmin):
    pass
