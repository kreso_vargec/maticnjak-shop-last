# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool

from filer.fields.image import FilerImageField


class PageImagesExtension(PageExtension):
    featured_image = FilerImageField(
        verbose_name=_('Featured Image'),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='+',
        help_text=_("An image that's used as a representation image of this page."),
    )

    def copy_relations(self, oldinstance, language):
        self.featured_image = oldinstance.featured_image


extension_pool.register(PageImagesExtension)
