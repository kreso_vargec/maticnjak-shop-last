# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template
from django.core.exceptions import ObjectDoesNotExist

register = template.Library()


@register.simple_tag
def get_page_image(page, name):
    """
    Returns the image while falling back to parent page images if empty.

    {% get_page_image request.current_page 'featured' as page_featured_image %}
    """
    while page:
        try:
            return getattr(page.pageimagesextension, '{}_image'.format(name))
        except (AttributeError, ObjectDoesNotExist):
            page = page.parent_page
