# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin
from shop.money.fields import MoneyField
from shopit.models.product import Product
from shopit.models.categorization import Category, Brand, Manufacturer
from shopit.models.flag import Flag
from shopit.models.modifier import Modifier


@python_2_unicode_compatible
class ProductSet(CMSPlugin):
    """
    Product set model for displaying filtered set of products.
    """
    title = models.CharField(_('Title'), max_length=255, blank=True)

    products = models.ManyToManyField(
        Product,
        blank=True,
        related_name='+',
        verbose_name=_('Products'),
        help_text=_('Select fixed products to display. If selected filters will be ignored.'),
    )

    # Filters by categorization
    categories = models.ManyToManyField(
        Category, blank=True, related_name='+', verbose_name=_('Categories'),
        help_text=_('Filter products in category'),
    )
    brands = models.ManyToManyField(
        Brand, blank=True, related_name='+', verbose_name=_('Brands'),
        help_text=_('Filter products in brand'),
    )
    manufacturers = models.ManyToManyField(
        Manufacturer, blank=True, related_name='+', verbose_name=_('Manufacturers'),
        help_text=_('Filter products in manufacturer'),
    )

    # Flags
    flags = models.ManyToManyField(
        Flag, blank=True, related_name='+', verbose_name=_('Flags'),
        help_text=_('Filter products by flags'),
    )

    # Modifiers
    modifiers = models.ManyToManyField(
        Modifier, blank=True, related_name='+', verbose_name=_('Modifiers'),
        help_text=_('Filter products by modifiers'),
    )

    # Price
    price_from = MoneyField(_('Price from'), blank=True, default=0, help_text=('Filter products over given price.'))
    price_to = MoneyField(_('Price to'), blank=True, default=0, help_text=('Filter products under given price.'))

    # Sort
    SORT_CHOICES = [
        ('name', _('Name A-Z')),
        ('-name', _('Name Z-A')),
        ('price', _('Price cheapest first')),
        ('-name', _('Price expensive first')),
    ]
    sort = models.CharField(_('Sort'), max_length=128, blank=True, choices=SORT_CHOICES)

    # Limit
    limit = models.PositiveIntegerField(_('Limit'), default=4, help_text=_('Limit number of products displayed.'))

    def __str__(self):
        return self.title if self.title else str(self.pk)

    def copy_relations(self, oldinstance):
        """Copy relations for m2m fields."""
        self.products = oldinstance.products.all()
        self.categories = oldinstance.categories.all()
        self.brands = oldinstance.brands.all()
        self.manufacturers = oldinstance.manufacturers.all()
        self.flags = oldinstance.flags.all()
        self.modifiers = oldinstance.modifiers.all()

    @property
    def as_dict(self):
        """
        Dictionary representation of product set filters.
        """
        data = {
            # 'title': str(self.title),
            'title': self.title,
            'products': None,
            'categories': None,
            'brands': None,
            'manufacturers': None,
            'flags': None,
            'modifiers': None,
            'price_from': None,
            'price_to': None,
            'sort': self.sort,
            'limit': self.limit,
        }

        if self.products.all():
            # Return just produts if given.
            data['products'] = self.products.all()
            sort_map = {
                'name': 'translations__name',
                '-name': '-translations__name',
                'price': '_unit_price',
                '-price': '-_unit_price',
            }
            sort = sort_map.get(self.sort, None)
            if sort:
                data['products'] = data['products'].order_by(sort)
            data['products'] = data['products'][:self.limit]
            return data

        if self.categories.all():
            data['categories'] = list(set(self.categories.all().values_list('translations__slug', flat=True)))
        if self.brands.all():
            data['brands'] = list(set(self.brands.all().values_list('translations__slug', flat=True)))
        if self.manufacturers.all():
            data['manufacturers'] = list(set(self.manufacturers.all().values_list('translations__slug', flat=True)))
        if self.flags.all():
            data['flags'] = list(set(self.flags.all().values_list('code', flat=True)))
        if self.modifiers.all():
            data['modifiers'] = list(set(self.modifiers.all().values_list('code', flat=True)))
        if self.price_from > 0:
            data['price_from'] = int(self.price_from.as_decimal())
        if self.price_to > 0:
            data['price_to'] = int(self.price_to.as_decimal())

        return data


# @python_2_unicode_compatible
# class FeaturedProducts(CMSPlugin):
#   title = models.CharField(
#     verbose_name=_('Title'),
#     max_length=128
#   )

#   products = models.ManyToManyField (
#     Product,
#     verbose_name=_('Products')
#   )

#   def __str__(self):
#         return self.title

# def get_products(self):
#     return self.products.all()
