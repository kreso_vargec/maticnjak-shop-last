# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.deprecation import MiddlewareMixin
from shop.middleware import CustomerMiddleware as OriginalCustomerMiddleware


class CustomerMiddleware(MiddlewareMixin, OriginalCustomerMiddleware):
    """
    Customer middleware adjusted for 'new style' Django middleware.
    """
