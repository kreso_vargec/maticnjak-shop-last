# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from shopit.models.modifier import Modifier

from .models import ProductSet


class ProductSetModelForm(forms.ModelForm):
    class Meta:
        model = ProductSet
        exclude = []

    # def __init__(self, *args, **kwargs):
    #     super(ProductSetModelForm, self).__init__(*args, **kwargs)
    #     self.fields['modifiers'].queryset = Modifier.objects.filtering_enabled().active()
