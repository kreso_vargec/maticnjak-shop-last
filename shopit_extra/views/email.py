# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from shop.models.notification import EmulateHttpRequest
from shop.serializers.order import OrderDetailSerializer
from shop.conf import app_settings as shop_settings

from shopit.models import Order


def email_test_view(request):
    """
    Used for testing the appearance of order email notification.
    """
    instance = Order.objects.filter_from_request(request).first()
    if not instance:
        return HttpResponse("There are no orders in this request!")

    emulated_request = EmulateHttpRequest(instance.customer, instance.stored_request)
    customer_serializer = shop_settings.CUSTOMER_SERIALIZER(instance.customer)
    order_serializer = OrderDetailSerializer(instance, context={'request': emulated_request})
    language = instance.stored_request.get('language')
    context = {
        'customer': customer_serializer.data,
        'data': order_serializer.data,
        'ABSOLUTE_BASE_URI': emulated_request.build_absolute_uri().rstrip('/'),
        'render_language': language,
    }

    return render(request, 'shopit/email/email_test.txt', context)
