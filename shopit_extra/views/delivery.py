# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework.views import APIView
from rest_framework.response import Response
from shopit.models.cart import Cart


class DeliveryUpdateView(APIView):
    """
    View that updates current cart with the delivery country data passed in
    through the `country_code` post param.
    """
    def post(self, request):
        country_code = request.POST.get('country_code', None)
        cart = Cart.objects.get_from_request(request)
        if country_code in [x[0] for x in settings.SHOPIT_ADDRESS_COUNTRIES]:
            cart.extra['delivery_country_code'] = country_code
            cart.save()
            return Response({'country_code': country_code})
        else:
            cart.extra.pop('delivery_country_code', None)
            cart.save()
            return Response({'country_code': None})
        return Response({'error': 'Invalid country code: {}'.format(country_code)}, status=400)
