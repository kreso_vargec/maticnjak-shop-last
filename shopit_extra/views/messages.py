# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from rest_framework import viewsets
from rest_framework.response import Response

from ..serializers.messages import MessageSerializer


class MessageInstance(object):
    """
    Message instance holder.
    """
    def __init__(self, message):
        self.message = message.message
        self.type = message.tags.split(' ')[-1]


class MessagesViewSet(viewsets.ViewSet):
    """
    Viewset that returns latest messages from django framework.
    """
    def list(self, request):
        storage = messages.get_messages(request._request)
        queryset = [MessageInstance(x) for x in storage]
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)
