# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from rest_framework.views import APIView
from rest_framework.response import Response
from shop.money import Money

from shopit.models.categorization import Category
from shopit.templatetags.shopit_tags import get_attributes, get_products


class CatalogFiltersView(APIView):
    """
    Renders catalog filters data. Accepts `category_id` through the GET data to
    limit what products filters are returned.
    """
    def get(self, request, format=None):
        try:
            category = Category.objects.active().get(id=request.GET['category_id'])
            cat_slugs = [x.safe_translation_getter('slug') for x in category.get_descendants(include_self=True)]
            products = get_products(categories=','.join(cat_slugs))
        except (KeyError, Category.DoesNotExist):
            products = None

        data = {
            'attributes': [],
            'price_range': [],
        }

        for attr in get_attributes(products):
            attr_data = attr.as_dict
            attr_data.update({'choices': [x.as_dict for x in attr.get_choices()]})
            data['attributes'].append(attr_data)

        for num in ['<100', '<250', '<500', '>500']:
            is_less_than, price = num[:1] == '<', Money(num[1:])
            price_data = {
                'display': _('do {price}').format(price=price) if is_less_than else 'preko {}'.format(price),
                'pt': int(price.as_decimal()) if is_less_than else None,
                'pf': int(price.as_decimal()) if not is_less_than else None,
            }
            data['price_range'].append(price_data)

        return Response(data)
