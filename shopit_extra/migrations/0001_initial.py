# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-05-03 11:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import shop.money.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '__first__'),
        ('shopit', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductSet',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='shopit_extra_productset', serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(blank=True, max_length=255, verbose_name='Title')),
                ('price_from', shop.money.fields.MoneyField(blank=True, help_text='Filter products over given price.', null=True, verbose_name='Price from')),
                ('price_to', shop.money.fields.MoneyField(blank=True, help_text='Filter products under given price.', null=True, verbose_name='Price to')),
                ('sort', models.CharField(blank=True, choices=[('name', 'Name A-Z'), ('-name', 'Name Z-A'), ('price', 'Price cheapest first'), ('-name', 'Price expensive first')], max_length=128, verbose_name='Sort')),
                ('limit', models.PositiveIntegerField(default=4, help_text='Limit number of products displayed.', verbose_name='Limit')),
                ('brands', models.ManyToManyField(blank=True, help_text='Filter products in brand', related_name='_productset_brands_+', to='shopit.Brand', verbose_name='Brands')),
                ('categories', models.ManyToManyField(blank=True, help_text='Filter products in category', related_name='_productset_categories_+', to='shopit.Category', verbose_name='Categories')),
                ('flags', models.ManyToManyField(blank=True, help_text='Filter products by flags', related_name='_productset_flags_+', to='shopit.Flag', verbose_name='Flags')),
                ('manufacturers', models.ManyToManyField(blank=True, help_text='Filter products in manufacturer', related_name='_productset_manufacturers_+', to='shopit.Manufacturer', verbose_name='Manufacturers')),
                ('modeifiers', models.ManyToManyField(blank=True, help_text='Filter products by modifiers', related_name='_productset_modeifiers_+', to='shopit.Modifier', verbose_name='Modifiers')),
                ('products', models.ManyToManyField(blank=True, help_text='Select fixed products to display. If selected filters will be ignored.', related_name='_productset_products_+', to='shopit.Product', verbose_name='Products')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
