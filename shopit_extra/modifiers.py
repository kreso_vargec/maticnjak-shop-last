# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from shop.modifiers.base import ShippingModifier, PaymentModifier
from shop.serializers.cart import ExtraCartRow

from shopit_extra.shipping import GLSShipping
from shopit_extra.payment import PayOnDelivery


class GLSShippingModifier(ShippingModifier):
    """
    Modifier that adds shipping to cart.
    """
    identifier = GLSShipping.namespace
    shipping_provider = GLSShipping()

    def get_choice(self):
        return (self.identifier, GLSShipping.label)

    def is_active(self, cart):
        """
        Make it always active, since it's the only shipping available.
        """
        return True

    def add_extra_cart_row(self, cart, request):
        if not self.is_active(cart):
            return
        instance = self.shipping_provider.get_shipping_instance(cart, request)
        cart.extra_rows[self.identifier] = ExtraCartRow(instance)
        cart.total += instance['amount']


class PayOnDeliveryModifier(PaymentModifier):
    identifier = PayOnDelivery.namespace
    payment_provider = PayOnDelivery()

    def get_choice(self):
        return (self.identifier, PayOnDelivery.label)

    def is_active(self, cart):
        """
        This modifier can only be active if delivery country is set to
        Croatia.
        """
        is_active = super(PayOnDeliveryModifier, self).is_active(cart)
        if not is_active:
            return False
        try:
            address = cart.shipping_address or cart.billing_address
            country = address.country
        except AttributeError:
            country = cart.extra.get('delivery_country_code', None)
        return country == 'HR'
    def add_extra_cart_row(self, cart, request):
        if not self.is_active(cart):
            return
    #    amount=cart.total*0.1
    #    instance = {
    #            'label':"10% popusta",
    #           'amount':amount
    #    }
    #    cart.extra_rows[self.identifier] = ExtraCartRow(instance)
    #    cart.total -= instance['amount']
