# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import transaction


def update_order_product_stock(order, restore=False):
    """
    Modifies product quntity value based on given order.
    Pass in `restore=True` for reversed effect.
    """
    with transaction.atomic():
        for item in order.items.all().select_related('product'):
            if item.product.quantity is not None:
                addition = item.quantity if restore else item.quantity * -1
                item.product.quantity = item.product.quantity + addition
                if item.product.quantity < 0:
                    item.product.quantity = 0
                item.product.save(update_fields=['quantity'])
