# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from shop.money import Money


def get_codes_for_zones(zones=[]):
    """
    Returns the codes for the given zone id's.
    """
    codes = []
    zones = [int(x) for x in zones]
    for country in settings.ADDRESS_COUNTRIES_ZONES:
        if country[2] in zones:
            codes.append(country[0])
    return codes


REGIONS = OrderedDict([
    ('hr', {
        'code': 'hr',
        'label': _('Dostava (Hrvatska)'),
        'label_short': _('Hrvatska'),
        'country_codes': ['HR'],
        'flat_rate': Money(30),
        'free_treshold': Money(300),
    }),

    ('si', {
        'code': 'si',
        'label': _('Dostava (Slovenia)'),
        'label_short': _('Slovenia'),
        'country_codes': ['SI'],
        'flat_rate': Money(63),
        'free_treshold': Money(850),
    }),

    ('at', {
        'code': 'at',
        'label': _('Dostava (Austria)'),
        'label_short': _('Austria'),
        'country_codes': ['AT'],
        'flat_rate': Money(139),
        'free_treshold': Money(2000),
    }),

    ('be', {
        'code': 'be',
        'label': _('Dostava (Belgium)'),
        'label_short': _('Belgium'),
        'country_codes': ['BE'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('cz', {
        'code': 'cz',
        'label': _('Dostava (Czech Republic)'),
        'label_short': _('Czech Republic'),
        'country_codes': ['CZ'],
        'flat_rate': Money(183),
        'free_treshold': Money(3500),
    }),

    ('dk', {
        'code': 'dk',
        'label': _('Dostava (Denmark)'),
        'label_short': _('Denmark'),
        'country_codes': ['DK'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('fi', {
        'code': 'fi',
        'label': _('Dostava (Finland)'),
        'label_short': _('Finland'),
        'country_codes': ['FI'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('fr', {
        'code': 'fr',
        'label': _('Dostava (France)'),
        'label_short': _('France'),
        'country_codes': ['FR'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('ie', {
        'code': 'ie',
        'label': _('Dostava (Ireland)'),
        'label_short': _('Ireland'),
        'country_codes': ['IE'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('it', {
        'code': 'it',
        'label': _('Dostava (Italy)'),
        'label_short': _('Italy'),
        'country_codes': ['IT'],
        'flat_rate': Money(183),
        'free_treshold': Money(3500),
    }),

    ('lu', {
        'code': 'lu',
        'label': _('Dostava (Luxembourg)'),
        'label_short': _('Luxembourg'),
        'country_codes': ['LU'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('hu', {
        'code': 'hu',
        'label': _('Dostava (Hungary)'),
        'label_short': _('Hungary'),
        'country_codes': ['HU'],
        'flat_rate': Money(75),
        'free_treshold': Money(1050),
    }),

    ('nl', {
        'code': 'nl',
        'label': _('Dostava (Netherlands)'),
        'label_short': _('Netherlands'),
        'country_codes': ['NL'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),


    ('de', {
        'code': 'de',
        'label': _('Dostava (Germany)'),
        'label_short': _('Germany'),
        'country_codes': ['DE'],
        'flat_rate': Money(139),
        'free_treshold': Money(2000),
    }),

    ('pl', {
        'code': 'pl',
        'label': _('Dostava (Poland)'),
        'label_short': _('Poland'),
        'country_codes': ['PL'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('pt', {
        'code': 'pt',
        'label': _('Dostava (Portugal)'),
        'label_short': _('Portugal'),
        'country_codes': ['PT'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('sk', {
        'code': 'sk',
        'label': _('Dostava (Slovakia)'),
        'label_short': _('Slovakia'),
        'country_codes': ['SK'],
        'flat_rate': Money(183),
        'free_treshold': Money(3800),
    }),


    ('es', {
        'code': 'es',
        'label': _('Dostava (Spain)'),
        'label_short': _('Spain'),
        'country_codes': ['ES'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    ('gb', {
        'code': 'gb',
        'label': _('Dostava (United Kingdom)'),
        'label_short': _('United Kingdom'),
        'country_codes': ['GB'],
        'flat_rate': Money(211),
        'free_treshold': Money(3800),
    }),

    # ('eu', {
    #     'code': 'eu',
    #     'label': _('Dostava (Europska Unija)'),
    #     'label_short': _('Europska Unija'),
    #     'country_codes': get_codes_for_zones([1, 2, 3]),
    #     'flat_rate': Money(160),
    #     'free_treshold': Money(1000),
    # }),

    ('rest-of-europe', {
        'code': 'rest-of-europe',
        'label': _('Dostava (Ostatak Europe)'),
        'label_short': _('Ostatak Europe'),
        'country_codes': get_codes_for_zones([4]),
        'flat_rate': Money(500),
        'free_treshold': Money(3800),
    }),
    ('sad-canada', {
        'code': 'sad-canada',
        'label': _('Dostava (SAD, Canada)'),
        'label_short': _('SAD, Canada'),
        'country_codes': get_codes_for_zones([5]),
        'flat_rate': Money(500),
        'free_treshold': Money(3800),
    }),
    ('world', {
        'code': 'world',
        'label': _('Dostava (Svijet)'),
        'label_short': _('Svijet'),
        'country_codes': get_codes_for_zones([6, 7]),
        'flat_rate': Money(500),
        'free_treshold': Money(3800),
    }),
])
