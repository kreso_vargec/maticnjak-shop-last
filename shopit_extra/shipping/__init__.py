# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import itertools

from django.utils.translation import ugettext_lazy as _
from shop.shipping.base import ShippingProvider
from shop.money import Money

from .regions import REGIONS


class GLSShipping(ShippingProvider):
    """
    GLS Shipping provider that's free after a set threshold is met.
    """
    namespace = 'gls-shipping'
    label = _('Dostava (izračun na plaćanju)')

    @classmethod
    def get_region(cls, cart, request):
        """
        Get region from shipping address on cart, or from cookie.
        If country not set returns None.
        """
        try:
            address = cart.shipping_address or cart.billing_address
            country = address.country
        except AttributeError:
            country = cart.extra.get('delivery_country_code', None)
        valid_codes = itertools.chain(*[x['country_codes'] for x in REGIONS.values()])
        valid_codes = list(set(valid_codes))
        if country not in valid_codes:
            return None
        for region in REGIONS.values():
            if country in region['country_codes']:
                return region

    @classmethod
    def get_shipping_instance(cls, cart, request):
        # Default instance.
        instance = {
            'label': cls.label,
            'amount': Money(0),
            'free_treshold': None,
            'label_with_amount': '{0} - <strong>{1}</strong>'.format(cls.label, Money(0))
        }

        region = cls.get_region(cart, request)
        if region:
            # If GLSShipping is set to cart, subtract the amount from total to
            # calculate the free_treshold on a total without shipping.
            if cls.namespace in cart.extra_rows:
                total = cart.total - cart.extra_rows[cls.namespace].instance['amount']
            else:
                total = cart.total
            # Set region data on instance.
            amount = Money(0) if total >= region['free_treshold'] else region['flat_rate']
            instance = {
                'label': region['label'],
                'amount': amount,
                'free_treshold': region['free_treshold'],
                'label_with_amount': '{0} - <strong>{1}</strong>'.format(region['label'], amount)
            }
        return instance

