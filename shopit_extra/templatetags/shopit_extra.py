# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from premailer import Premailer

from django.template import Library
from django.utils.html import mark_safe
from django.utils import translation
from django.template.loader import render_to_string

from shop.conf import app_settings as shop_settings
from shopit.templatetags.shopit_tags import order as order_tag

from ..serializers.checkout import CheckoutSerializer

from shopit.models import Product
import random

register = Library()


@register.filter
def og_money(value):
    """
    Filter that formats money to facebook's OG format.
    """
    value = str(value.as_decimal()).replace(',', '.').split('.')
    return '{}.{}'.format(''.join(value[:-1]), value[-1])


@register.simple_tag(takes_context=True)
def get_serialized_product(context, product, many=False):
    """
    Returns product serialized data.
    """
    serializer_class = shop_settings.PRODUCT_SUMMARY_SERIALIZER
    serializer = serializer_class(product, many=many, context={'request': context['request']})
    return serializer.data


@register.simple_tag(takes_context=True)
def get_serialized_checkout(context):
    """
    Returns checkout data serialized.
    """
    return CheckoutSerializer(context=context).data


@register.simple_tag(takes_context=True)
def order_email(context, number, is_admin=False):
    """
    Wrapper for `order` templatetag that adds email values to context, as well
    as inlines the styles.
    """
    data = order_tag(context, number=number)

    data.update({
        'mail': True,
        'ABSOLUTE_BASE_URI': context['ABSOLUTE_BASE_URI'],
        'is_admin': is_admin,
    })

    translation.activate(context['render_language'])
    style = render_to_string('shopit/email/style.css')
    html = render_to_string('shopit/includes/order.html', data)
    merged = '<style>{0}</style>{1}'.format(style, html)
    inlined = Premailer(merged, strip_important=False).transform()
    return mark_safe(inlined)


@register.simple_tag(takes_context=True)
def get_random_products_from_category(context, category, limit=4):
    related = Product.objects.filter(_category__in=category.get_descendants(include_self=True))
    if 'product' in context:
        related = related.exclude(id=context['product'].id)
    related = sorted(related[:limit * 10], key=lambda x: random.random())
    return related[:limit]
