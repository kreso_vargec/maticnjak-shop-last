# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url

from . import rest_api
from shop.urls import auth
from shop.urls import payment


urlpatterns = [
    url(r'^api/', include(rest_api)),
    url(r'^auth/', include(auth)),
    url(r'^payment/', include(payment)),
]

# Add test email url when in DEBUG mode.
if settings.DEBUG:
    from shopit_extra.views.email import email_test_view
    urlpatterns += [url(r'^email-test/', email_test_view)]
