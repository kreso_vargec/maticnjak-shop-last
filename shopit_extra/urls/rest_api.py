# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include
from rest_framework import routers
from shop.urls.rest_api import urlpatterns as shop_urlpatterns
from shop.views.cart import CartViewSet, WatchViewSet
from shop.views.checkout import CheckoutViewSet

from shopit_extra.views.catalog import CatalogFiltersView
from shopit_extra.views.messages import MessagesViewSet
from shopit_extra.views.delivery import DeliveryUpdateView

router = routers.DefaultRouter()
router.register(r'cart', CartViewSet, base_name='cart')
router.register(r'watch', WatchViewSet, base_name='watch')
router.register(r'checkout', CheckoutViewSet, base_name='checkout')
router.register(r'messages', MessagesViewSet, base_name='messages')

# Skip last url to re-add the modified router urls.
urlpatterns = shop_urlpatterns[:-1] + [
    url(r'^catalog_filters/$', CatalogFiltersView.as_view(), name='get-catalog-filters'),
    url(r'^delivery_country_code/$', DeliveryUpdateView.as_view(), name='delivery-country-code'),
    url(r'^', include(router.urls)),
]
