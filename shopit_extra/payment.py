# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django_fsm import transition
from shop.payment.base import PaymentProvider
from shop.models.order import OrderModel, OrderPayment
from shop_wspay.payment import WSPayWorkflowMixin

from shopit_extra.utils import update_order_product_stock


class PayOnDelivery(PaymentProvider):
    label = _('Plaćanje pouzećem')
    namespace = 'pay-on-delivery'

    def get_payment_request(self, cart, request):
        order = OrderModel.objects.create_from_cart(cart, request)
        order.populate_from_cart(cart, request)
        order.awaiting_payment_on_delivery()
        order.save()
        cart.delete()
        return 'window.location.href="{}";'.format(reverse('shopit-thanks'))


class PayOnDeliveryWorkflowMixin(object):
    """
    Workflow mixin that adds transitions for PayOnDelivery payment.
    """
    TRANSITION_TARGETS = {
        'awaiting_payment_on_delivery': _("Awaiting payment on delivery"),
        'payment_on_delivery_confirmed': _("Payment on delivery confirmed"),
    }

    def is_fully_paid(self):
        return super(PayOnDeliveryWorkflowMixin, self).is_fully_paid()

    @transition(field='status', source=['created'], target='awaiting_payment_on_delivery', custom=dict(admin=False))
    def awaiting_payment_on_delivery(self):
        """Signals that order is shipped, and awaiting payment once it arrives."""
        update_order_product_stock(self)

    @transition(
        field='status', source=['awaiting_payment_on_delivery'], target='payment_on_delivery_confirmed',
        conditions=[is_fully_paid], custom=dict(admin=True, button_name=_('Acknowledge Payment on delivery')))
    def acknowledge_payment_on_delivery(self):
        """Change status to `payment_on_delivery_confirmed` once paid on delivery."""


class ModifiedWSPayWorkflowMixin(WSPayWorkflowMixin):
    """
    Modified `WSPayWorkflowMixin` that updates product quantity on purchase.
    """
    TRANSITION_TARGETS = WSPayWorkflowMixin.TRANSITION_TARGETS

    @transition(field='status', source=['created'], target='paid_with_wspay', custom=dict(admin=False))
    def add_wspay_payment(self, transaction_id, amount):
        assert self.currency == 'HRK', "Currency need's to be in HRK"
        OrderPayment.objects.create(
            order=self,
            amount=amount,
            transaction_id=transaction_id,
            payment_method='wspay',
        )
        update_order_product_stock(self)


class CancelOrderWorkflowMixin(object):
    """
    Add this class to `settings.SHOP_ORDER_WORKFLOWS` to mix it into your `OrderModel`.
    It adds all the methods required for state transitions, to cancel an order.
    """
    TRANSITION_TARGETS = {
        'order_canceled': _("Order Canceled"),
    }

    def cancelable(self):
        """Cancel only if no amount paid already."""
        return not self.amount_paid

    @transition(
        field='status', source=['awaiting_payment_on_delivery'], target='order_canceled',
        conditions=[cancelable], custom=dict(admin=True, button_name=_("Cancel Order")))
    def cancel_order(self):
        """Signals that an Order shall be canceled."""
        update_order_product_stock(self, restore=True)
