# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.encoding import smart_text
from rest_framework import serializers

from shopit_extra.shipping.regions import REGIONS


class CheckoutInstance(object):
    """
    Checkout placeholder object. Takes in context, and relies on checkout
    forms to be availible in the context (It should only be used on Shopit's
    checkout page).
    """
    forms = {}

    def __init__(self, context):
        self.customer = context.get('customer', None)
        self.forms['customer_form'] = context.get('customer_form', None)
        self.forms['shipping_form'] = context.get('shipping_form', None)
        self.forms['billing_form'] = context.get('billing_form', None)
        self.forms['payment_form'] = context.get('payment_form', None)
        self.forms['delivery_form'] = context.get('delivery_form', None)
        self.forms['extra_form'] = context.get('extra_form', None)
        self.forms['accept_form'] = context.get('accept_form', None)

        if self.customer and all(self.forms.values()):
            self.set_data()

    def get_initial(self):
        initial = {}
        for form in self.forms.values():
            for field in form:
                value = field.initial
                if field.html_name in ['shipping-existant', 'billing-existant']:
                    value = getattr(value, 'id', "")
                elif field.html_name in ['guest-phone_number', 'customer-phone_number']:
                    value = smart_text(value)
                elif field.html_name == 'accept-accept':
                    value = False
                initial[field.html_name] = value
        return initial

    def set_data(self):
        self.initial = self.get_initial()
        self.is_guest = not self.customer.is_registered()
        if not self.is_guest:
            self.salutations = [x for x in self.forms['customer_form'].fields['salutation'].choices]
        self.existant_shipping_addresses = [x for x in self.forms['shipping_form'].fields['existant'].choices]
        self.existant_billing_addresses = [x for x in self.forms['billing_form'].fields['existant'].choices]
        self.countries = [x for x in self.forms['shipping_form'].fields['country'].choices]
        self.payment_methods = [x for x in self.forms['payment_form'].fields['payment_modifier'].choices]
        self.delivery_methods = [x for x in self.forms['delivery_form'].fields['shipping_modifier'].choices]
        self.delivery_regions = [x for x in REGIONS.values()]


class CheckoutSerializer(serializers.Serializer):
    """
    Checkout serializer.
    """
    is_guest = serializers.BooleanField(read_only=True)
    initial = serializers.DictField(read_only=True)
    salutations = serializers.ListField(read_only=True)
    existant_shipping_addresses = serializers.ListField(read_only=True)
    existant_billing_addresses = serializers.ListField(read_only=True)
    countries = serializers.ListField(read_only=True)
    payment_methods = serializers.ListField(read_only=True)
    delivery_methods = serializers.ListField(read_only=True)
    delivery_regions = serializers.ListField(read_only=True)

    def __init__(self, *args, **kwargs):
        instance = CheckoutInstance(kwargs['context'])
        super(CheckoutSerializer, self).__init__(instance, *args, **kwargs)
