# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.encoding import smart_text
from rest_framework import serializers


class MessageSerializer(serializers.Serializer):
    """
    Message serializer.
    """
    message = serializers.CharField(read_only=True)
    type = serializers.CharField(read_only=True)
