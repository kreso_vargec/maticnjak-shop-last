# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from shopit.models.order import Order
from shopit.models.product import Product


def count_products(request):
    return Product.objects.filter(kind__in=[Product.SINGLE, Product.GROUP]).count()


def count_new_orders(request):
    return Order.objects.filter(
        status__in=[
            'awaiting_payment',
            'payment_confirmed',
            'awaiting_payment_on_delivery',
            'paid_with_wspay',
        ], created_at__gte='2018-05-07').count()
