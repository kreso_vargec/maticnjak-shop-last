# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import ProductSet
# from .models import ProductSet, FeaturedProducts
from .forms import ProductSetModelForm


@plugin_pool.register_plugin
class ProductSetPlugin(CMSPluginBase):
    model = ProductSet
    form = ProductSetModelForm
    name = _('Product Set')
    render_template = 'shopit_extra/product_set.html'
    raw_id_fields = ['products']

    fieldsets = [
        (None, {'fields': ['title']}),
        (_('Products'), {'fields': ['products']}),
        (_('Settings'), {'fields': [('sort', 'limit')]}),
    ]

    def render(self, context, instance, placeholder):
        context = super(ProductSetPlugin, self).render(context, instance, placeholder)
        context.update({'data': instance.as_dict})
        return context


@plugin_pool.register_plugin
class FeaturedProductsPlugin(CMSPluginBase):
     model = ProductSet
     name = _('Featured products')
     render_template = 'shopit_extra/featured_products.html'
