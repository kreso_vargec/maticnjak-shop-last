# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from .views import DealershipFormView


urlpatterns = [
    url('form/', DealershipFormView.as_view(), name='dealership-form'),
]
