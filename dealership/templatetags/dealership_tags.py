# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.template import Library

from rest_framework.renderers import JSONRenderer

from utils.templatetags.utils_cms import get_page
from utils.templatetags.utils_url import get_absolute_url_with_fallback

from ..serializers import DealershipFormSerializer

register = Library()


@register.simple_tag(takes_context=True)
def get_dealership_form_data(context):
    serializer = DealershipFormSerializer()
    thanks_page = get_page(context, 'thanks')

    data = {
        'url': reverse('dealership-form'),
        'success_url': get_absolute_url_with_fallback(thanks_page),
        'fields': {},
        'labels': {
            'submit': _('Pošalji'),
            'submit_loading': _('Šalje se'),
        },
    }

    for field in serializer.fields.values():
        field_data = {
            'label': field.label,
            'help_text': field.help_text,
        }
        if hasattr(field, 'choices'):
            field_data['choices'] = field.choices
        data['fields'][field.field_name] = field_data
    return JSONRenderer().render(data).decode('utf-8')
