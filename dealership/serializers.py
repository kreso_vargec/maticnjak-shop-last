# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import smtplib

from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from post_office import mail

from rest_framework import serializers
from cms.models import Page

from utils.templatetags.utils_url import get_absolute_url_with_fallback

from local_utils import COUNTRIES

class DealershipFormSerializer(serializers.Serializer):
    LABELS = {
        'company': _('Naziv firme'),
        'oib': _('OIB'),
        'address': _('Address'),
        'zip_code': _('Poštanski broj'),
        'city': _('Grad'),

        'state': _('Država'),

        'phone': _('Broj telefona'),
        'email': _('E-mail'),
        'message': _('Vaša poruka'),
        'accept': _('Prihvaćam <a href="%s">Politiku privatnosti</a>.'),
    }

    company = serializers.CharField(label=LABELS['company'])
    oib = serializers.CharField(label=LABELS['oib'])
    address = serializers.CharField(label=LABELS['address'])
    zip_code = serializers.CharField(label=LABELS['zip_code'])
    city = serializers.CharField(label=LABELS['city'])

    state_choices = COUNTRIES
    state = serializers.ChoiceField(
        label=LABELS['state'],
        required=False,
        choices=state_choices,
        help_text=_('Izaberite državu'),
    )

    phone = serializers.CharField(label=LABELS['phone'], required=False)
    email = serializers.EmailField(label=LABELS['email'])
    message = serializers.CharField(label=LABELS['message'], help_text=_('Npr. Molim vas R1 račun'))
    accept = serializers.BooleanField(label=LABELS['accept'], required=True)

    def __init__(self, *args, **kwargs):
        super(DealershipFormSerializer, self).__init__(*args, **kwargs)
        terms_page = Page.objects.filter(reverse_id='terms_and_conditions').first()
        if terms_page:
            self.fields['accept'].label = self.LABELS['accept'] % get_absolute_url_with_fallback(terms_page)

    def send_mail(self, request):
        subject = '%s%s' % (settings.EMAIL_SUBJECT_PREFIX, _('Zastupnistvo'))


        body = ''
        for field, label in self.LABELS.items():
            if field == 'accept':
                continue
            value = self.data.get(field, '-')
            body += '{0}:\n{1}\n\n'.format(label, value)


        message = mail.send(
            recipients=[x[1] for x in settings.MANAGERS],
            subject=subject,
            message=body,
            headers={'Reply-to': self.data['email']},
            context=self.data,
        )

        try:
            return bool(message)
        except smtplib.SMTPException:
            return False
