import money from '../money'

export function getItemSubtotal (item) {
  return money.format(money.unformat(item.summary.price) * item.quantity)
}
