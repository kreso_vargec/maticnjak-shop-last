import Vue from 'vue'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

import Form from 'element-ui/lib/form'
import FormItem from 'element-ui/lib/form-item'
import Input from 'element-ui/lib/input'
import Checkbox from 'element-ui/lib/checkbox'
import CheckboxGroup from 'element-ui/lib/checkbox-group'
import Radio from 'element-ui/lib/radio'
import RadioGroup from 'element-ui/lib/radio-group'
import Select from 'element-ui/lib/select'
import Option from 'element-ui/lib/option'
import DatePicker from 'element-ui/lib/date-picker'

import Tooltip from 'element-ui/lib/tooltip'
import Loading from 'element-ui/lib/loading'

import Message from 'element-ui/lib/message'

locale.use(lang)

Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Checkbox)
Vue.use(CheckboxGroup)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(Select)
Vue.use(Option)
Vue.use(DatePicker)

Vue.use(Loading)
Vue.use(Tooltip)

Vue.message = Message
Vue.prototype.$message = Message
