import Vue from "vue";
import axios from "axios";
import Cookies from "js-cookie";
import { VueHammer } from "vue2-hammer";
import VueScrollTo from "vue-scrollto";

import "./directives";
import "./element-ui";

import VueDjangoCMSPatch from "./patches/vue-djangocms-patch";
import router from "./components/router";
import store from "./components/store";
import components from "./components";

import "../sass/main.scss";

// // Refercene GSAP plugins.

const App = window.App;

// Set ignored elements as required by vue-djangocms-patch.
Vue.config.ignoredElements = ["cms-template", "cms-plugin"];

// Set $config & $labels from `App`.
Vue.prototype.$config = App.config;
Vue.prototype.$labels = App.labels;

// Configure `Axios` as Vue http service.
Cookies.set("csrftoken", App.config.csrf, {
  expires: 1
});
axios.defaults.headers.post["X-CSRFToken"] = App.config.csrf;
axios.defaults.headers.delete["X-CSRFToken"] = App.config.csrf;
axios.defaults.headers.post["X-Requested-With"] = "XMLHttpRequest";
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded; charset=UTF-8";
Vue.axios = Vue.prototype.axios = Vue.prototype.$http = axios;

Vue.use(VueHammer);
Vue.use(VueScrollTo);

new Vue({
  el: "#app",
  router,
  store,
  components,
  data: {
    disableScroll: false,
    isLoaded: false,

    showMiniCart: false,
    showSearch: false
  },
  watch: {
    disableScroll(newValue) {
      if (newValue) document.body.style.overflow = "hidden";
      else document.body.style.removeProperty("overflow");
    },

    showMiniCart(newValue) {
      if (newValue && this.isMobile) {
        this.disableScroll = true;
      } else {
        this.disableScroll = false;
      }
    },
    showSearch(newValue) {
      this.disableScroll = newValue;
    }
  },
  created() {
    new VueDjangoCMSPatch(this);

    // Fetch cart data.
    this.$store.dispatch("cart/fetch");

    // Fetch messages.
    this.$store.dispatch("messages/fetch");
  },
  mounted() {
    this.preloadStuff();
  },
  methods: {
    preloadStuff() {
      setTimeout(() => {
        this.isLoaded = true;
      }, 100);
    }
  }
});
