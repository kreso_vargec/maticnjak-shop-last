import get from 'lodash/get'
import { parseNumber, isValidNumber } from 'libphonenumber-js'

export default {
  data () {
    return {
      fromRulesMatchPasswordField: 'password',
      formRules: {
        required: {required: true, message: this.$labels.form.errors.required},
        email: {type: 'email', message: this.$labels.form.errors.email},
        isTrue: {validator: (rule, value, callback) => {
          if (value !== true) callback(new Error(this.formRules.required.message))
          else callback()
        }},
        phoneNumber: {validator: (rule, value, callback) => {
          if (isValidNumber(value) !== true) callback(new Error(this.$labels.form.errors.phone))
          else callback()
            callback()
        }},
        matchPassword: {validator: (rule, value, callback) => {
          if (this.formData && this.formData[this.fromRulesMatchPasswordField] && this.formData[this.fromRulesMatchPasswordField] === value) callback()
          else callback(new Error(this.$labels.form.passwordMissmatch))
        }},
        serverError: {validator: (rule, value, callback) => {
          let error = get(this.formServerErrors, rule.field, false)
          if (error) callback(new Error(error[0]))
          else callback()
        }}
      }
    }
  }
}
