import _ from 'lodash/core'
import { mapState } from 'vuex'
import qs from 'qs'

import { getItemSubtotal } from '../../helpers/cart-helpers'

export default {
  props: {
    item: {type: Object, required: true},
    cartDisabled: {type: Boolean, default: false}
  },
  data () {
    return {
      disabled: false
    }
  },
  computed: {
    ...mapState('cart', [
      'status'
    ]),
    product () { return this.item.summary },
    previewImage () { return _.first(this.product.attachments.images || []) },
    previewImageDefaultUrl () { return this.$config.catalog.defaultProductImageUrlSmall },
    isAvailable () { return this.product.is_available[0] },
    itemId () { return parseInt(this.item.url.split('/').slice(-2, -1).pop(), 10) },
    isDisabled () { return this.cartDisabled || this.disabled },
    lineSubtotal () { return this.getItemSubtotal(this.item) },
    total () { return this.lineSubtotal  }
  },
  watch: {
    status (newValue) {
      if (newValue === 'success' || newValue === 'error') {
        this.disabled = false
      }
    }
  },
  methods: {
    getItemSubtotal,
  update (addition) {
      if (this.disabled) return
      if (!this.isAvailable && addition > 0) return
      this.disabled = true
      let data = {}
      this.$message({message: this.$labels.cart.addToAdding, type: 'adding'})
      this.$emit('adding');
      data[`quantity-${this.itemId}`] = this.item.quantity + addition
      data = qs.stringify(data, {indices: false})
      this.axios.post(this.$config.shop.urls.cart, data).then(response => {
        this.$store.dispatch('messages/fetch')
        this.$store.dispatch('cart/fetch')
      })
    }
  }
}
