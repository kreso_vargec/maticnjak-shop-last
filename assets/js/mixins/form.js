import _ from 'lodash/core'
import qs from 'qs'

import formRulesMixin from './form-rules'

export default {
  mixins: [formRulesMixin],
  props: {
    formDataInitial: {type: Object, default: null},
    initial: {
      type: Object,
      default: null
    }
  },
  data() {
    return {
      formDisabled: false,
      formRef: 'form',
      formData: {},
      formServerErrors: {}
    }
  },
  computed: {
    form() {
      return this.$refs[this.formRef]
    },
    formActionUrl() {
      return window.location.pathname
    },
    formDataFormated() {
      return this.formData
    },
    formDataFormData() {
      let data = new FormData()
      _.each(this.formDataFormated, (value, key) => {
        // Handle multiple choice fields
        if (_.isArray(value))
          _.each(value, subValue => data.append(key, subValue))
        else data.append(key, value)
      })
      return data
    },

    formDataSerialized () { return qs.stringify(this.formDataFormated, {indices: false}) }
  },
  created() {
    this.formData = this.formInitial()

    if (this.formDataInitial) this.formData = _.clone(this.formDataInitial)
  },
  methods: {
    formInitial() {
      return this.initial ? _.clone(this.initial) : {}
    },
    formSubmit() {
      if (!this.formDisabled) {
        let isValid = this.formValidate()
        if (isValid) this.formValid()
        else this.formInvalid()
      }
    },
    formValidate() {
      return true
    },
    formValid() {
      this.formDisabled = true
      this.formShowLoading()

      this.axios
        .post(this.formActionUrl, this.formDataFormData)
        .then(response => _.delay(() => this.formSuccess(response.data), 1000))
        .catch(error =>
          _.delay(() => this.formError(error.response.data), 1000)
        )
    },
    formInvalid() {
      this.formMessage('invalid')
    },
    formSuccess(data) {
      this.formMessage('success')
    },
    formError(data) {
      this.formDisabled = false
      this.formHideLoading()
      this.formServerErrors = data
      this.formErrorResetData()
      this.formValidate()

      // Show either server error, or a generic one.
      if (this.formServerErrors['non_field_errors'])
        this.formMessage(
          'error',
          _.first(this.formServerErrors['non_field_errors'])
        )
      else this.formMessage('error')
    },
    formErrorResetData() {
      // Not implemented by default.
    },
    formClearServerError(key) {
      if (_.has(this.formServerErrors, key)) delete this.formServerErrors[key]
    },
    formShowLoading() {
      // Not implemented by default.
    },
    formHideLoading() {
      // Not implemented by default.
    },
    formMessage(type, message) {
      // Not implemented by default.
    }
  }
}
