import _ from 'lodash/core'
import qs from 'qs'

import formRulesMixin from './form-rules-mixin'

export default {
  mixins: [formRulesMixin],
  props: {
    formDataInitial: {type: Object, default: null}
  },
  data () {
    return {
      formDisabled: false,
      formLoading: null,
      formRef: 'form',
      formData: {},
      formServerErrors: {}
    }
  },
  computed: {
    form () { return this.$refs[this.formRef] },
    formActionUrl () { return window.location.pathname },
    formDataFormated () { return this.formData },
    formDataSerialized () { return qs.stringify(this.formDataFormated, {indices: false}) }
  },
  created () {
    if (this.formDataInitial) this.formData = _.clone(this.formDataInitial)
  },
  methods: {
    formSubmit () {
      if (!this.formDisabled) {
        this.form.validate(valid => valid ? this.formValid() : this.formInvalid())
      }
    },
    formValid () {
      this.formDisabled = true
      this.formLoading = this.$loading({target: this.$el})

      this.axios
        .post(this.formActionUrl, this.formDataSerialized)
        .then(response => _.delay(() => this.formSuccess(response.data), 1000))
        .catch(error => _.delay(() => this.formError(error.response.data), 1000))
    },
    formInvalid () {
      this.$message({message: this.$labels.form.errors.correct, type: 'error'})
    },
    formSuccess (data) {
      window.location.reload()
    },
    formError (data) {
      this.formLoading.close()
      this.formDisabled = false
      this.formServerErrors = data
      this.formErrorResetData()
      this.form.validate()

      // Show either server error, or a generic one.
      if (this.formServerErrors['non_field_errors']) this.$message({message: _.first(this.formServerErrors['non_field_errors']), type: 'error'})
      else this.$message({message: this.$labels.form.errors.correct, type: 'error'})
    },
    formErrorResetData () {
      // Not implemented by default.
    },
    formClearServerError (key) {
      if (_.has(this.formServerErrors, key)) delete this.formServerErrors[key]
    }
  }
}
