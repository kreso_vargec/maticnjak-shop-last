import _ from 'lodash/core'
import get from 'lodash/get'
import imagesLoaded from 'imagesloaded'
import { mapGetters, mapState } from 'vuex'

export default {
  data () {
    return {
      selectedOptions: {},
      variants: [],
      attributeChoices: {},
      localCartQuantity: null,
      loaded: false
    }
  },
  computed: {
    images () { return this.currentProduct.attachments.images || [] },
    previewImage () { return _.first(this.images) },
    otherImages () { return this.images.slice(1) },
    imagesCount () { return this.images.length },
    previewImageDefaultUrl () { return this.$config.catalog.defaultProductImageUrl },
    firstFlag () { return _.first(_.filter(this.product.flags, (o) => { return o.path.startsWith('oznake/') })) },
    selectedVariant () {
      let selected = null
      _.each(this.variants, (variant) => {
        let valid = true
        _.each(variant.attributes, (attr) => { if (this.selectedOptions[attr.code] !== attr.value) valid = false })
        if (valid) { selected = variant; return false }
      })
      return selected
    },
    currentProduct () { return this.selectedVariant ? this.selectedVariant : this.product },
    currentCartItem () { return this.$store.getters['cart/getItemByProductId'](this.currentProduct.id) },
    isAvailable () { return this.currentCartItem ? this.currentCartItem.summary.is_available[0] : this.currentProduct.quantity != 0 },
    canBeRemoved () { return this.localCartQuantity === null || this.currentCartItem && this.currentCartItem.quantity === this.localCartQuantity },
    cartQuantity () { return this.currentCartItem ? this.currentCartItem.quantity : this.localCartQuantity }
  },
  watch: {
    selectedOptions: {
      handler () { this.localCartQuantity = null },
      deep: true
    }
  },
  mounted () {

  },
  methods: {
    removeFromCart () {
      if (this.canBeRemoved) {
        this.localCartQuantity = 0
        this.axios.delete(this.currentCartItem.url).then(response => {
          this.$store.dispatch('cart/fetch')
          this.$message({message: this.$labels.cart.removedFrom, type: 'info'})
        })
      }
    }
  }
}
