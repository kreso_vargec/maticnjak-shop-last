import _ from 'lodash/core'
import Vue from 'vue'

export default {
  // Filter allowed params.
  filterCartParams (params) {
    return _.pick(params, ['fields', 'include'])
  },

  // Returns cart items.
  getCartItems (params={}, callback) {
    params = this.filterCartParams(params)

    Vue.axios
      .get('/shop/api/cart/?fields=is_available', {params})
      // /shop/api/cart/?fields=is_available
      .then(response => callback(response.data))
      .catch(error => callback(null))
  },

  // Add item to cart.
  addToCart (url, quantity, extra, callback) {
    let params = {'quantity': quantity}
    if (extra) params['extra'] = extra
    Vue.axios
      .post(url, params)
      .then(response => callback(response.data))
      .catch(error => callback(null))
  },

  // Add item to watchlist.
  addToWatch (url, quantity, callback) {

  }
}
