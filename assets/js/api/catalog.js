import _ from 'lodash/core'
import Vue from 'vue'


export default {
  // Filter allowed params.
  filterProductsParams (params) {
    return _.pick(params, _.concat(['limit', 'offset', 'pf', 'pt', 'c', 'b', 'm', 'f', 'd', 's'], App.config.catalog.productsAttributeCodes))
  },

  // Fetch products filtered by params.
  getProducts (params, callback) {
    params = this.filterProductsParams(params)
    params['format'] = 'json'

    Vue.axios
      .get(App.config.catalog.urls.products, {params})
      .then(response => callback(response.data))
      .catch(error => callback(null))
  },

  // Fetch total products count filtered by params.
  getProductsCount (params, callback) {
    params = this.filterProductsParams(params)
    params['format'] = 'json'
    params['get_count'] = 1

    Vue.axios
      .get(App.config.catalog.urls.products, {params})
      .then(response => callback(response.data.count))
      .catch(error => callback(null))
  }
}
