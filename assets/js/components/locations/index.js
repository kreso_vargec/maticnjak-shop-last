import Vue from 'vue'

import WebShops from './WebShops'
Vue.component(WebShops.name, WebShops)

import Stores from './Stores'
Vue.component(Stores.name, Stores)
