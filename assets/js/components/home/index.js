import Vue from 'vue'

import HomeProductCategories from './HomeProductCategories'
Vue.component(HomeProductCategories.name, HomeProductCategories)

import HomeProducts from './HomeProducts'
Vue.component(HomeProducts.name, HomeProducts)

import HomeFormLinks from './HomeFormLinks'
Vue.component(HomeFormLinks.name, HomeFormLinks)

import HomeRecipies from './HomeRecipies'
Vue.component(HomeRecipies.name, HomeRecipies)

import HomeTestimonials from './HomeTestimonials'
Vue.component(HomeTestimonials.name, HomeTestimonials)

import HomeNews from './HomeNews'
Vue.component(HomeNews.name, HomeNews)
