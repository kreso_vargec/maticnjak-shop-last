import Vue from 'vue'

import Account from './Account'
Vue.component(Account.name, Account)

import AccountItem from './AccountItem'
Vue.component(AccountItem.name, AccountItem)

import AccountOrderList from './AccountOrderList'
Vue.component(AccountOrderList.name, AccountOrderList)

import AccountOrderItems from './AccountOrderItems'
Vue.component(AccountOrderItems.name, AccountOrderItems)

import AccountOrderDetail from './AccountOrderDetail'
Vue.component(AccountOrderDetail.name, AccountOrderDetail)

import AccountDetailsForm from './AccountDetailsForm'
Vue.component(AccountDetailsForm.name, AccountDetailsForm)

import AccountPasswordForm from './AccountPasswordForm'
Vue.component(AccountPasswordForm.name, AccountPasswordForm)

import LoginRegisterForm from './LoginRegisterForm'
Vue.component(LoginRegisterForm.name, LoginRegisterForm)

import PasswordResetForm from './PasswordResetForm'
Vue.component(PasswordResetForm.name, PasswordResetForm)
