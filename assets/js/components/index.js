import Vue from 'vue'

import Search from './Search'
Vue.component(Search.name, Search)

import BrowserUpgrade from './BrowserUpgrade'
Vue.component(BrowserUpgrade.name, BrowserUpgrade)

import './header'
import './blocks'
import './locations'
import './recipes'
import './modules'
import './blogit'
import './contact'
import './media'
import './catalog'
import './shop'
import './home'
import './account'

export default {}