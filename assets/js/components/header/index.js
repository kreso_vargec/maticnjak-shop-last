import Vue from 'vue'

import Navbar from './Navbar'
Vue.component(Navbar.name, Navbar)

import MainMenu from './MainMenu'
Vue.component(MainMenu.name, MainMenu)

import MainMenuItem from './MainMenuItem'
Vue.component(MainMenuItem.name, MainMenuItem)

import CatalogMenu from './CatalogMenu'
Vue.component(CatalogMenu.name, CatalogMenu)

import Login from './Login'
Vue.component(Login.name, Login)

import Logo from './Logo'
Vue.component(Logo.name, Logo)
