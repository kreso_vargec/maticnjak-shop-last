import Vue from 'vue'

import ContactInfo from './ContactInfo'
Vue.component(ContactInfo.name, ContactInfo)

import ContactForm from './ContactForm'
Vue.component(ContactForm.name, ContactForm)

import DealershipForm from './DealershipForm'
Vue.component(DealershipForm.name, DealershipForm)
