import Vue from 'vue'

import ShoppingCart from './ShoppingCart'
Vue.component(ShoppingCart.name, ShoppingCart)

import CartSummary from './CartSummary'
Vue.component(CartSummary.name, CartSummary)

import CartSummaryItem from './CartSummaryItem'
Vue.component(CartSummaryItem.name, CartSummaryItem)

import CartIcon from './CartIcon'
Vue.component(CartIcon.name, CartIcon)

import CheckoutForm from './CheckoutForm'
Vue.component(CheckoutForm.name, CheckoutForm)

import CheckoutPage from './CheckoutPage'
Vue.component(CheckoutPage.name, CheckoutPage)

import AddToCart from './AddToCart'
Vue.component(AddToCart.name, AddToCart)

import ShopThanks from './ShopThanks'
Vue.component(ShopThanks.name, ShopThanks)

import MiniCart from './MiniCart'
Vue.component(MiniCart.name, MiniCart)
