<template>
  <div class="cart-summary-item">
    <a
      href="#"
      class="cart-quantity"
      :class="{'disabled': isDisabled}"
      :key="`${item.product}-${item.quantity}`"
      @click.prevent="update(item.quantity * -1)"
      v-if="item.quantity > 0">
      <span>{{ item.quantity }}</span>
      <span>X</span>
    </a>

    <div class="image">
      <a :href="product.url" class="image-link">
        <img :src="previewImage.url" :alt="previewImage.label" widht="75" height="95">
      </a>
    </div>

    <div class="name">
      <a :href="product.url">{{ product.name }}
        <span v-for="flag in product.flags" :key="flag.id">{{ flag.code }}</span>
      </a>
      <div class="info">
        <span class="price" v-if="item.quantity > 1">({{ product.price }} x{{ item.quantity }})  =</span>
        <span class="total">{{ lineSubtotal }}</span>
      </div>
    </div>
  </div>
</template>

<script>
import cartItemMixin from '../../mixins/shop/cart-item-mixin'

export default {
  name: 'MiniCartItem',
  mixins: [cartItemMixin],
}
</script>

<style lang="scss" scoped>
@import "assets/sass/_resources.scss";

.cart-summary-item {
  display: flex;
  justify-items: center;
  align-items: center;
  margin-bottom: spacing(2);
  padding-bottom: spacing(2);
  border-bottom: 1px solid rgba($black, .15);
}

.cart-quantity {
  @include trans(background-color);
  @include size(24px);
  @include plumber-size('p-ultra-small');
  position: relative;
  color: var(--dark-gray);
  background-color: var(--orange);
  border-radius: 100%;
  display: block;
  margin-right: spacing();

  @include grid('desktop') {
    @include size(32px);
  }

  &:hover {
    background-color: var(--black);

    span {
      &:first-child {
        opacity: 0;
        visibility: hidden;
      }

      &:last-child {
        opacity: 1;
        visibility: visible;
      }
    }
  }

  span {
    @include trans((opacity, visibility));
    position: absolute;
    color: var(--dark-gray);
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-weight: 500;

    &:last-child {
      opacity: 0;
      visibility: hidden;
      color: var(--white);
    }
  }
}

.image {
  margin-right: spacing();
}

.image-link {
  @include trans(opacity);

  &:hover {
    opacity: .65;
  }
}

.name {
  a {
    @include trans(color);
    @include plumber-size('p');
    padding: 0 !important;
    margin: 0 !important;
    color: var(--green);
    font-weight: 700;

    &:hover {
      color: $action-color;
    }

    span {
      color: var(--dark-gray);
      font-weight: 500;
      padding-right: spacing(.25);
    }
  }

  .info {
    color: var(--dark-gray);

    .price {
      @include plumber-size('p');
      padding: 0 !important;
      margin: 0 !important;
    }

    .total {
      @include plumber-size('p');
      font-weight: 700;
      padding: 0 !important;
      margin: 0 !important;
    }
  }
}
</style>
