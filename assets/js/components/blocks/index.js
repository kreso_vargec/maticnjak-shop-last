import Vue from 'vue'

import ImageBlock from './ImageBlock'
Vue.component(ImageBlock.name, ImageBlock)

import GalleryBlock from './GalleryBlock'
Vue.component(GalleryBlock.name, GalleryBlock)

import QuoteBlock from './QuoteBlock'
Vue.component(QuoteBlock.name, QuoteBlock)

import VideoBlock from './VideoBlock'
Vue.component(VideoBlock.name, VideoBlock)

import DocumentBlock from './DocumentBlock'
Vue.component(DocumentBlock.name, DocumentBlock)

import ImageWithContent from './ImageWithContent'
Vue.component(ImageWithContent.name, ImageWithContent)

import Facts from './Facts'
Vue.component(Facts.name, Facts)
