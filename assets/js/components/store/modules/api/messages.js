import _ from 'lodash/core'
import Vue from 'vue'


export default {
  // Return new messages from django messages framework.
  getMessages (callback) {
    Vue.axios
      .get(App.config.messages.urls.get)
      .then(response => callback(response.data))
      .catch(error => callback(null))
  }
}
