import _ from 'lodash/core'
import Vue from 'vue'


export default {
  // Filter allowed params.
  filterSearchParams (params) {
    return _.pick(params, ['limit', 'offset', 'text', 'q', 'autocomplete'])
  },

  // Get results.
  getResults (params, callback) {
    params = this.filterSearchParams(params)
    params['format'] = 'json'

    Vue.axios
      .get(App.config.search.urls.get, {params})
      .then(response => callback(response.data))
      .catch(error => callback(null))
  }
}
