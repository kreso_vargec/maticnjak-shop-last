import _ from 'lodash/core'
import Vue from 'vue'
import debounce from 'lodash/debounce'

import shop from './api/shop'

const state = {
  // Cart data.
  items: {},
  totalQuantity: null,
  numItems: null,
  subtotal: null,
  total: null,
  extraRows: [],

  params: App.config.shop.cartParams,
  status: null
}

const getters = {
  getItemByProductId: (state) => (id) => {
    return _.first(_.filter(state.items, item => item.product === id))
  }
}

const mutations = {
  setData (state, data) {
    state.items = data.items
    state.totalQuantity = data.total_quantity
    state.numItems = data.num_items
    state.subtotal = data.subtotal
    state.total = data.total
    state.extraRows = data.extra_rows
  },
  setStatus (state, status) { state.status = status },
  setParams (state, params) { state.params = shop.filterCartParams(params) },
}

const actions = {
  fetch: debounce(({ state, commit }, params) => {
    if (params) commit('setParams', params)
    commit('setStatus', 'fetching')
    shop.getCartItems(state.params, data => {
      if (data) { commit('setData', data) }
      commit('setStatus', data ? 'success' : 'error')
    })
  }, 1000),
  addToCart ({ commit, dispatch }, payload) {

    var url = payload['url']
    var quantity = payload['quantity']
    var extra = payload['extra']

    commit('setStatus', 'addingToCart')
    return new Promise((resolve, reject) => {
      shop.addToCart(url, quantity, extra, data => {
        if (data) {
          commit('setStatus', 'addToCartSuccess')
          dispatch('fetch')
          resolve(data)
        } else {
          commit('setStatus', 'addToCartError')
          reject()
        }
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
