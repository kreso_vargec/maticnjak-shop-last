import _ from 'lodash/core'
import debounce from 'lodash/debounce'
import get from 'lodash/get'

import catalog from './api/catalog'

const state = {
  // Products data.
  count: null,
  next: null,
  previous: null,
  results: [],

  params: {},
  status: null
}

const getters = {}

const mutations = {
  // Set or append to state data.
  setData (state, data) {
    let append = get(data, 'append', false)
    state.count = data.count
    state.next = data.next
    state.previous = data.previous
    state.results = append ? _.concat(state.results, data.results) : data.results
  },
  setStatus (state, status) { state.status = status },
  setParams (state, params) { state.params = catalog.filterProductsParams(params) },
  updateParams (state, params) {
    let newParams = _.clone(state.params)
    _.each(catalog.filterProductsParams(params), (value, key) => { newParams[key] = value })
    state.params = newParams
  }
}

const actions = {
  fetch: debounce(({ state, commit }, params) => {
    if (params) commit('setParams', params)
    commit('setStatus', 'fetching')
    catalog.getProducts(state.params, data => {
      if (data) { commit('setData', data) }
      commit('setStatus', data ? 'success' : 'error')
    })
  }, 500),
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
