import _ from 'lodash/core'
import uniq from 'lodash/uniq'
import Vue from 'vue'
import debounce from 'lodash/debounce'

import messages from './api/messages'

const state = {
  queued: [],
  displayed: []
}

const getters = {}

const mutations = {
  addToQueue (state, messages) { state.queued = _.concat(state.queued, messages) },
  clearQueue (state) {
    state.displayed = _.concat(state.displayed, uniq(state.queued))
    state.queued = []
  }
}

const actions = {
  fetch: debounce(({ commit, dispatch }) => {
    messages.getMessages(data => {
      if (data && data.length) {
        commit('addToQueue', data)
        dispatch('display')
      }
    })
  }, 1000),
  display ({ state, commit }) {
    _.each(uniq(state.queued), message => Vue.message(message))
    commit('clearQueue')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
