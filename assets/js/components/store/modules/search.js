import _ from 'lodash/core'
import debounce from 'lodash/debounce'
import get from 'lodash/get'

import search from './api/search'

const state = {
  // Search data
  count: null,
  next: null,
  previous: null,
  results: [],

  params: {},
  status: null
}

const getters = {}

const mutations = {
  setData (state, data) {
    let append = get(data, 'append', false)
    state.count = data.count
    state.next = data.next
    state.previous = data.previous
    state.results = append ? _.concat(state.results, data.results) : data.results
  },
  setStatus (state, status) { state.status = status },
  setParams (state, params) { state.params = search.filterSearchParams(params) },
  updateParams (state, params) {
    let newParams = _.clone(state.params)
    _.each(search.filterSearchParams(params), (value, key) => { newParams[key] = value })
    state.params = newParams
  }
}

const actions = {
  fetch: debounce(({ commit, dispatch }, params) => {
    if (params) commit('setParams', params)
    commit('setStatus', 'fetching')
    search.getResults(state.params, data => {
      if (data) { commit('setData', data) }
      commit('setStatus', data ? 'success' : 'error')
    })
  }, 500),
  reset ({ commit }) {
    commit('setStatus', null)
    commit('setData', {count: null, next: null, previous: null, results: []})
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
