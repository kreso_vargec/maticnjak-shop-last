import Vue from 'vue'
import Vuex from 'vuex'

import cart from './modules/cart'
import messages from './modules/messages'
import products from './modules/products'
import search from './modules/search'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    cart,
    messages,
    products,
    search
  }
})
