import Vue from 'vue'

import Recipes from './Recipes'
Vue.component(Recipes.name, Recipes)

import RecipesDetail from './RecipesDetail'
Vue.component(RecipesDetail.name, RecipesDetail)
import RecipeItem from './RecipeItem'
Vue.component(RecipeItem.name, RecipeItem)
