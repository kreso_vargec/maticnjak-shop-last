import Vue from 'vue'

import PostCategories from './PostCategories'
Vue.component(PostCategories.name, PostCategories)

import PostList from './PostList'
Vue.component(PostList.name, PostList)

import PostDetail from './PostDetail'
Vue.component(PostDetail.name, PostDetail)

import PostItem from './PostItem'
Vue.component(PostItem.name, PostItem)

import PostSidebar from './PostSidebar'
Vue.component(PostSidebar.name, PostSidebar)

import PostMenu from './PostMenu'
Vue.component(PostMenu.name, PostMenu)
