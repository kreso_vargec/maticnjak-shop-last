import Vue from 'vue'

import ProductHeadContent from './ProductHeadContent'
Vue.component(ProductHeadContent.name, ProductHeadContent)

import ProductList from './ProductList'
Vue.component(ProductList.name, ProductList)

import ProductListItem from './ProductListItem'
Vue.component(ProductListItem.name, ProductListItem)

import ProductDetail from './ProductDetail'
Vue.component(ProductDetail.name, ProductDetail)

import ProductSet from './ProductSet'
Vue.component(ProductSet.name, ProductSet)

import ProductCategories from './ProductCategories'
Vue.component(ProductCategories.name, ProductCategories)
