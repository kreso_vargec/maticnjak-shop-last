import Vue from 'vue'

import MediaList from './MediaList'
Vue.component(MediaList.name, MediaList)

import MediaItem from './MediaItem'
Vue.component(MediaItem.name, MediaItem)
