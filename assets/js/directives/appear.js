import _ from 'lodash/core'
import ScrollMagic from 'ScrollMagic'
import Vue from 'vue'

/**
 * Used to appear elements by adding a class as they come into view.
 * Override defaults by specifying `window.APPEAR_DEFAULTS = {}` object.
 *
 * <div v-appear.toggle.children="{
 *   'offset': 150,
 *   'hook': 'onEnter',
 *   'delay': 100,
 *   'class': 'show',
 *   'selector': 'h2, h3, h4',
 *   'enabled': isAppearEnabled
 * }"></div>
 */

const _appearDefaults = {
  immediate: false,
  toggle: false,
  children: false,
  offset: 10,
  hook: 'onEnter',
  delay: 0,
  class: 'appear',
  selector: null
}

Vue.directive('appear', {
  bind(el, binding, vnode) {
    vnode.appear = {
      controller: null,
      setup(el, binding) {
        let config = _.clone(binding.modifiers)
        if (_.isObject(binding.value)) _.defaults(config, binding.value)
        _.defaults(config, window.APPEAR_DEFAULTS || {}, _appearDefaults)

        if (!config.immediate) this.controller = new ScrollMagic.Controller()

        let hook = el => {
          if (config.immediate) el.classList.add(config.class)
          else {
            let scene = new ScrollMagic.Scene({
              triggerElement: el,
              triggerHook: config.hook,
              offset: config.offset
            })
            scene.setClassToggle(el, config.class)
            scene.addTo(this.controller)
            if (!config.toggle) scene.on('start', () => scene.destroy())
          }
        }

        _.delay(() => {
          if (config.children) {
            let children = config.selector ? el.querySelectorAll(config.selector) : el.children
            _.each(children, hook)
          } else hook(el)
        }, config.delay)
      }
    }
  },
  inserted(el, binding, vnode) {
    let enable = _.has(binding.value, 'enabled') ? binding.value.enabled : true
    if (enable) vnode.appear.setup(el, binding)
  },
  update(el, binding, vnode, oldVnode) {
    vnode.appear = oldVnode.appear
    let enabled = _.has(binding.oldValue, 'enabled') ? binding.oldValue.enabled : true
    let enable = _.has(binding.value, 'enabled') ? binding.value.enabled : true
    if (!enabled && enable) vnode.appear.setup(el, binding)
    if (enabled && !enable) vnode.appear.controller.destroy(true)
  }
})
