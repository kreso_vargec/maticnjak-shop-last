import Vue from 'vue'

Vue.directive('click-animation', (el, binding) => {
  let className = binding.value ? binding.value : 'click'
  el.addEventListener('click', () => {
    el.classList.add(className)
  })
  el.addEventListener('animationend', () => {
    el.classList.remove(className)
  })
})
