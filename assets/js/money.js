import { formatMoney } from 'accounting-js'
import { formatNumber } from 'accounting-js'
import { unformat } from 'accounting-js'

const settings = {
  currency: {
    symbol: 'kn',
    format: '%v %s',
    decimal: ',',
    thousand: '.',
    precision: 2
  },
  number: {
    precision: 0,
    tousand: '.',
    decimal: ','
  }
}

export default {
  format (value) { return formatMoney(value, settings.currency) },
  unformat (value) { return unformat(value, settings.currency.decimal) },
  number (value) { return formatNumber(value, settings.number) }
}
