from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.toolbar.items import Break, SubMenu
from django.core.urlresolvers import reverse

from media.models import Media

try:
    from cms.cms_toolbars import ADMIN_MENU_IDENTIFIER, ADMINISTRATION_BREAK
except ImportError:
    from cms.cms_toolbar import ADMIN_MENU_IDENTIFIER, ADMINISTRATION_BREAK


class MediaToolbar(CMSToolbar):
    supported_apps = (
        'media',
    )

    watch_models = [Media]

    def populate(self):
        admin_menu = self.toolbar.get_menu(ADMIN_MENU_IDENTIFIER)

        if admin_menu:
            position = admin_menu.get_alphabetical_insert_position(_('Media'), SubMenu)
            if not position:
                position = admin_menu.find_first(Break, identifier=ADMINISTRATION_BREAK) + 1
                admin_menu.add_break('media-break', position=position)

            menu = admin_menu.get_or_create_menu('media-admin-menu', _('Media'), position=position)

            menu.add_sideframe_item(_('Media list'), url=reverse('admin:media_media_changelist'))
            menu.add_modal_item(_('Add new media'), url=reverse('admin:media_media_add'))
            menu.add_break()

        if self.is_current_app:
            current_menu = self.toolbar.get_or_create_menu('media-current-menu', _('Media'))
            add_menu = current_menu.get_or_create_menu('media-current-menu-add', _('Add New'))
            add_menu.add_modal_item(_('Media'), url=reverse('admin:media_media_add'))

toolbar_pool.register(MediaToolbar)
