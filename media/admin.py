from django.contrib import admin
from parler.admin import TranslatableAdmin
from cms.admin.placeholderadmin import FrontendEditableAdminMixin, PlaceholderAdminMixin
from adminsortable2.admin import SortableAdminMixin
from media.models import Media


@admin.register(Media)
class MediaAdmin(SortableAdminMixin, FrontendEditableAdminMixin, PlaceholderAdminMixin, TranslatableAdmin):
    frontend_editable_fields = [
        'name',
    ]

    list_display = ['name']

    readonly_fields = ['created', 'modified']

    fieldsets = [
        (None, {'fields': ['name', 'link', 'featured_image']}),
        (None, {'fields': [('created', 'modified')]})
    ]
