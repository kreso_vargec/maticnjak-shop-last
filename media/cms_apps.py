# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from media.views import MediaListView


class MediaApphook(CMSApp):
    name = _('Media')

    def get_urls(self, page=None, language=None, **kwargs):
        return [
            url(r'^$', MediaListView.as_view()),
        ]

apphook_pool.register(MediaApphook)
