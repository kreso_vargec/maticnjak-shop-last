from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, NoReverseMatch
from parler.models import TranslatableModel, TranslatedFields
from parler.utils.context import switch_language
from filer.fields.image import FilerImageField
from cms.utils.i18n import get_current_language


@python_2_unicode_compatible
class Media(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(_('Name'), max_length=128),
    )

    link = models.URLField(
        verbose_name=_('Link'),
        blank=True,
        null=True,
    )

    featured_image = FilerImageField(
        verbose_name=_('Featured image'),
        related_name='+',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    created = models.DateTimeField(
        verbose_name=_('Created'),
        auto_now_add=True
    )

    modified = models.DateTimeField(
        verbose_name=_('Modified'),
        auto_now=True
    )

    order = models.PositiveIntegerField(
        verbose_name=_('Sort'),
        default=0
    )

    class Meta:
        verbose_name = _('Media')
        verbose_name_plural = _('Medias')
        ordering = ['order']

    def __str__(self):
        return self.safe_translation_getter('name', any_language=True)
