# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-11-28 11:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='media',
            name='link',
            field=models.URLField(blank=True, null=True, verbose_name='Link'),
        ),
    ]
