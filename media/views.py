# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import ListView, DeleteView
from parler.views import TranslatableSlugMixin

from media.models import Media


class MediaListView(ListView):
    model = Media
    template_name = 'media/media_list.html'
    paginate_by = 18

    def get_queryset(self):
        return self.model.objects.all()
