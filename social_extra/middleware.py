# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from social_django.middleware import SocialAuthExceptionMiddleware as OriginalSocialAuthExceptionMiddleware


class SocialAuthExceptionMiddleware(OriginalSocialAuthExceptionMiddleware):
    """
    Modified social auth middleware.
    """
    def raise_exception(self, request, exception):
        return False

    def get_message(self, request, exception):
        return _('Došlo je do greške kod povezivanja.')
