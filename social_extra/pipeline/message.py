# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.contrib import messages
from django.utils.translation import ugettext_lazy as _


def success_message(*args, **kwargs):
    """
    Social auth pipeline that adds message to the django messages framework.
    """
    request, user = kwargs['request'], kwargs['user']

    if user.is_authenticated():
        messages.add_message(request, messages.SUCCESS, _('You have been successfully logged in.'))
