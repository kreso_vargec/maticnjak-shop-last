# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from shopit.models.customer import Customer


def create_customer(strategy, details, backend, user=None, *args, **kwargs):
    """
    Replacement for `social_core.pipeline.user.create_user` that creates user
    with customer.
    """
    request = kwargs['request']

    if user:
        user.customer.recognize_as_registered(request)
        return {'is_new': False}

    # Create customer.
    customer = Customer.objects.get_or_create_from_request(request)
    customer.email = details.get('email')
    customer.user.is_active = True
    customer.user.set_password(get_user_model().objects.make_random_password())
    customer.user.first_name = details.get('first_name', '')
    customer.user.last_name = details.get('last_name', '')
    customer.save()
    customer.recognize_as_registered(request)

    return {
        'is_new': True,
        'user': customer.user,
    }
